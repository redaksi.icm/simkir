-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Sep 2020 pada 16.54
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simkir`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `gedung`
--

CREATE TABLE `gedung` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_lokasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `gedung`
--

INSERT INTO `gedung` (`id`, `nama`, `alamat`, `kode_lokasi`) VALUES
(24, 'Rumah Bagonjong', 'Jln. Jenderal sudriman No 51 Padang', '11.03.00.04.08.03.02'),
(25, 'Istana Bung Hatta', 'Bukittinggi', '11.03.00.04.08.02.02'),
(26, 'Auditorium', 'jln jenderal sudriman no 52 padang', '11,03.00.04.01.01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_barang`
--

CREATE TABLE `master_barang` (
  `id` int(255) NOT NULL,
  `kode_barang` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merek` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_seri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ukuran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thn_pembelian` int(4) DEFAULT NULL,
  `harga` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `master_barang`
--

INSERT INTO `master_barang` (`id`, `kode_barang`, `nama`, `merek`, `no_seri`, `ukuran`, `bahan`, `thn_pembelian`, `harga`) VALUES
(13, '123.657', 'Lemari Es', 'Panasonic', '02.06.02.04.01', '', 'campuran', 2010, '3000000'),
(14, '0206010406', 'Brankas Besi', 'Cobra', '123.001', 'Menengah', 'Besi', 1995, '9000000'),
(15, '0206020128', 'kursi Tamu', 'Jepara/Ukira', '02.06.02.01.28', '-', 'Kayu', 2009, '12000000'),
(19, '123.655', 'Layar Infokus', 'Rangka Besi', '02.07.01.03.09', '96x96', 'Campuran', 2015, '23000000'),
(20, '123.656', 'Sound System', 'Yamaha', '02.06.02.06.08', '-', 'Metal/Fiber', 2015, '59400000'),
(22, '123.658', 'Kursi Pejabat Eselon II', 'Jepara/Ukira', '02.06.02.06.40', '-', 'kayu', 2015, '10000000'),
(23, '123.000', 'kursi Kerja', 'Jepara/Ukira', '02.06.02.06.50', '-', 'kayu', 2012, '5600000'),
(24, '123.65.78', 'Meja Kerja', 'Olimpik', '02.06.02.06.40', '-', 'kayu', 2017, '1000000'),
(25, '02.06.01.05.10', 'White Board Biasa', 'sakura', '-', '240x10', 'campuran', 2012, '2500000'),
(26, '02.06.02.01.11', 'Meja Pejabat Eselon IV', '', '', '', 'kaya/kaca', 2014, '1620000'),
(27, '02.06.02.01.31', 'Meja Staf rt', '', '', '1/2 Biro', 'kayu/kaca', 2017, '741000'),
(28, '02.06.02.01.30', 'Kursi Staf rt', '', '', '', 'campuran', 2009, '17550000'),
(29, '02.06.02.01.45', 'Whiteboard', 'Olimpik', '02060201343', 'Medium', 'Kayu', 2020, '10000000'),
(30, '02.06.02.01.79', 'Smartboard', 'Polytron', '3242355', '-', 'Plastik', 2020, '25000000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_ruangan`
--

CREATE TABLE `tbl_ruangan` (
  `id_ruangan` int(200) NOT NULL,
  `id` int(200) DEFAULT NULL,
  `nama_ruangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_ruangan`
--

INSERT INTO `tbl_ruangan` (`id_ruangan`, `id`, `nama_ruangan`) VALUES
(9, 24, 'Assisten Administrasi Umum '),
(10, 24, 'Tata Usaha & Keuangan'),
(11, 25, 'Aula Besar'),
(12, 26, 'Aula Pertemuan'),
(13, 24, 'Rumah Tangga & Protokol'),
(14, 27, 'Ruang Meeting Escape Buillding');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `kode_user` int(20) NOT NULL,
  `nip_user` varchar(100) DEFAULT NULL,
  `nama_user` varchar(100) DEFAULT NULL,
  `jabatan` varchar(20) DEFAULT NULL,
  `password` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`kode_user`, `nip_user`, `nama_user`, `jabatan`, `password`) VALUES
(7, 'admin', 'admin', 'admin', '827ccb0eea8a706c4c34a16891f84e7b'),
(9, 'user', 'user', 'user', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_barang_perbaikan`
--

CREATE TABLE `tb_barang_perbaikan` (
  `id_barang_perbaikan` int(255) NOT NULL,
  `id_perbaikan` int(255) DEFAULT NULL,
  `id` int(255) DEFAULT NULL,
  `ket` text DEFAULT NULL,
  `id_ruangan` int(255) DEFAULT NULL,
  `jumlah` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_barang_perbaikan`
--

INSERT INTO `tb_barang_perbaikan` (`id_barang_perbaikan`, `id_perbaikan`, `id`, `ket`, `id_ruangan`, `jumlah`) VALUES
(9, 4, 2, '-', 5, '1'),
(12, 4, 3, 'Rusak', 6, '1'),
(13, 4, 12, '-', 9, '1'),
(14, 4, 19, '-', 11, '1'),
(16, 6, 27, 'Rusak Ringan', 13, '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_keputusan`
--

CREATE TABLE `tb_keputusan` (
  `id_keputusan` int(255) NOT NULL,
  `nomor_keputusan` varchar(255) DEFAULT NULL,
  `tentang` varchar(255) DEFAULT NULL,
  `tanggal_keputusan` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_keputusan`
--

INSERT INTO `tb_keputusan` (`id_keputusan`, `nomor_keputusan`, `tentang`, `tanggal_keputusan`, `status`) VALUES
(5, '12.45.457', 'Penghapusan Barang', '2020-06-13', 'Simpan'),
(6, '140/200/Umum-2020', 'Penghapusan ', '2020-06-13', 'Simpan'),
(7, '140/009/Umum-2019', 'eerer', '2020-06-15', 'Simpan'),
(8, '140/014/Umum-2019', 'Penghapusan barang istana bung hatta', '2020-08-06', 'Simpan'),
(9, '140/910/Umum-2019', 'Penghapusan  Barang Istana Bung Hatta', '2020-08-09', 'Simpan'),
(10, '100/0011/Umum-2020', 'Penghapusan Meja Staf rt', '2020-09-12', 'Lihat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kib`
--

CREATE TABLE `tb_kib` (
  `id_kib` int(255) NOT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `daftar` text DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kib`
--

INSERT INTO `tb_kib` (`id_kib`, `nomor`, `daftar`, `tanggal`, `status`) VALUES
(7, '6374284643', 'kljlk', '2020-06-13', 'Lihat'),
(8, '100/009/aset-2020', 'Penghapusan Kendaran Motor Supra', '2020-09-11', 'Lihat'),
(9, '100/010/Umum-2020', 'Pindah Barang', '2020-09-12', 'Lihat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kir`
--

CREATE TABLE `tb_kir` (
  `id_kir` int(255) NOT NULL,
  `id_ruangan` int(200) DEFAULT NULL,
  `id` int(200) DEFAULT NULL,
  `jumlah_register` double DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `recon` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kir`
--

INSERT INTO `tb_kir` (`id_kir`, `id_ruangan`, `id`, `jumlah_register`, `keterangan`, `recon`) VALUES
(29, 7, 9, 3, '-', '2020'),
(32, 9, 12, 1, '-', '2020'),
(34, 10, 13, 1, '', '2020'),
(38, 10, 15, 4, '-', '2020'),
(48, 10, 23, 34, '-', '2020'),
(49, 11, 15, 34, '-', '2020'),
(50, 11, 19, 1, '-', '2020'),
(51, 11, 20, 1, '-', '2020'),
(52, 11, 22, 1, '', '2020'),
(53, 11, 14, 1, '-', '2020'),
(54, 9, 24, 4, '', '2020'),
(55, 9, 13, 1, '', '2020'),
(56, 9, 19, 1, '', '2020'),
(57, 12, 24, 34, '', '2020'),
(58, 12, 15, 4, '', '2020'),
(59, 12, 19, 1, '', '2020'),
(60, 13, 28, 9, '', '2020'),
(61, 13, 25, 1, '', '2020'),
(62, 13, 27, 9, '', '2020'),
(63, 13, 26, 1, '', '2020'),
(64, 13, 19, 1, '', '2020'),
(65, 14, 29, 10, '-', '2020'),
(66, 14, 27, 40, '-', '2020'),
(67, 13, 30, 1, '-', '2020');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_mutasi`
--

CREATE TABLE `tb_mutasi` (
  `id_mutasi` int(255) NOT NULL,
  `id_kib` int(255) DEFAULT NULL,
  `id_kir` int(200) DEFAULT NULL,
  `mutasi_ke` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_mutasi`
--

INSERT INTO `tb_mutasi` (`id_mutasi`, `id_kib`, `id_kir`, `mutasi_ke`) VALUES
(12, 7, 25, 'Aula Gadang'),
(13, 7, 29, 'Aula Gadang'),
(14, 7, 31, 'Aula Besar'),
(15, 7, 39, 'Assisten Administrasi Umum '),
(16, 7, 54, 'Rumah Tangga & Protokol'),
(18, 9, 49, 'Assisten Administrasi Umum ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pengapusan`
--

CREATE TABLE `tb_pengapusan` (
  `id_pengapusan` int(255) NOT NULL,
  `id_keputusan` int(255) DEFAULT NULL,
  `id` int(200) DEFAULT NULL,
  `nilai` varchar(255) DEFAULT NULL,
  `akumulasi_penyusutan` varchar(255) DEFAULT NULL,
  `nilai_buku` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pengapusan`
--

INSERT INTO `tb_pengapusan` (`id_pengapusan`, `id_keputusan`, `id`, `nilai`, `akumulasi_penyusutan`, `nilai_buku`) VALUES
(13, 6, 12, '100000', '100000', '100000'),
(14, 7, 15, '100000', '10000', '10000'),
(15, 9, 14, '100000', '500000', '100000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_perbaikan`
--

CREATE TABLE `tb_perbaikan` (
  `id_perbaikan` int(255) NOT NULL,
  `nomor_perbaikan` varchar(255) DEFAULT NULL,
  `tentang` text DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_perbaikan`
--

INSERT INTO `tb_perbaikan` (`id_perbaikan`, `nomor_perbaikan`, `tentang`, `tanggal`, `status`) VALUES
(4, '32433543545', 'Perbaikkan', '2020-06-13', 'Lihat'),
(6, '100/0010/Umum-2020', 'Perbaiakan satu meja rt', '2020-09-12', 'Lihat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_recon`
--

CREATE TABLE `tb_recon` (
  `id_recon` int(200) NOT NULL,
  `id_kir` int(200) DEFAULT NULL,
  `kondisi_baik` double DEFAULT NULL,
  `kondisi_kurang_baik` double DEFAULT NULL,
  `kondisi_rusak` double DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `tahun_recon` year(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_recon`
--

INSERT INTO `tb_recon` (`id_recon`, `id_kir`, `kondisi_baik`, `kondisi_kurang_baik`, `kondisi_rusak`, `keterangan`, `tahun_recon`) VALUES
(22, 25, 4, 0, 0, '-', 2020),
(23, 24, 1, 0, 0, '-', 2020),
(24, 29, 3, 0, 0, '-', 2020),
(25, 31, 0, 0, 0, '-', 2020),
(26, 32, 1, 0, 0, '-', 2020),
(27, 33, 0, 0, 0, '', 2020),
(28, 33, 0, 0, 0, '', 2020),
(29, 37, 0, 0, 0, '', 2020),
(30, 36, 0, 0, 0, '-', 2020),
(31, 35, 0, 0, 0, '-', 2020),
(32, 33, 0, 0, 0, '-', 2020),
(33, 38, 0, 0, 0, '', 2020),
(34, 34, 0, 0, 0, '', 2020),
(35, 39, 0, 0, 0, '', 2020),
(36, 31, 0, 0, 0, '', 2020),
(37, 35, 0, 0, 2, '', 2020),
(38, 40, 0, 0, 2, '', 2020),
(39, 40, 0, 0, 2, '', 2020),
(40, 41, 0, 0, 3, '', 2020),
(41, 41, 0, 0, 1, '', 2020),
(42, 37, 0, 0, 1, '', 2020),
(43, 36, 0, 0, 1, '', 2020),
(44, 44, 0, 0, 0, '', 2020),
(45, 43, 0, 0, 0, '', 2020),
(46, 42, 0, 0, 0, '', 2020),
(47, 45, 0, 0, 0, '-', 2020),
(48, 46, 0, 0, 0, '-', 2020),
(49, 47, 0, 0, 0, '-', 2020),
(50, 48, 0, 0, 0, '', 2020),
(51, 45, 0, 0, 0, '', 2020),
(52, 46, 0, 0, 2, '', 2020),
(53, 47, 0, 0, 1, '', 2020),
(54, 51, 0, 0, 1, '', 2020),
(55, 50, 1, 0, 0, '', 2020),
(57, 49, 34, 0, 0, '', 2020),
(58, 52, 0, 1, 0, '', 2020),
(59, 53, 0, 0, 1, '', 2000),
(60, 31, 0, 10, 0, '', 2020),
(61, 39, 0, 10, 0, '', 2020),
(62, 56, 0, 1, 0, '', 2020),
(63, 55, 0, 1, 0, '', 2020),
(66, 54, 4, 0, 0, '', 2000),
(67, 57, 34, 0, 0, '', 2020),
(68, 58, 4, 0, 0, '', 2020),
(69, 59, 1, 0, 0, '', 2020),
(70, 64, 1, 0, 0, '', 2020),
(71, 63, 1, 0, 0, '', 2020),
(72, 62, 1, 0, 0, '', 2020),
(73, 62, 9, 0, 0, '', 2020),
(74, 61, 1, 0, 0, '', 2020),
(75, 60, 9, 0, 0, '', 2020),
(76, 66, 40, 0, 0, '-', 2020),
(77, 65, 20, 0, 0, '-', 2020),
(78, 67, 0, 1, 0, '-', 2020);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `gedung`
--
ALTER TABLE `gedung`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `master_barang`
--
ALTER TABLE `master_barang`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_barang` (`kode_barang`);

--
-- Indeks untuk tabel `tbl_ruangan`
--
ALTER TABLE `tbl_ruangan`
  ADD PRIMARY KEY (`id_ruangan`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`kode_user`);

--
-- Indeks untuk tabel `tb_barang_perbaikan`
--
ALTER TABLE `tb_barang_perbaikan`
  ADD PRIMARY KEY (`id_barang_perbaikan`);

--
-- Indeks untuk tabel `tb_keputusan`
--
ALTER TABLE `tb_keputusan`
  ADD PRIMARY KEY (`id_keputusan`);

--
-- Indeks untuk tabel `tb_kib`
--
ALTER TABLE `tb_kib`
  ADD PRIMARY KEY (`id_kib`);

--
-- Indeks untuk tabel `tb_kir`
--
ALTER TABLE `tb_kir`
  ADD PRIMARY KEY (`id_kir`);

--
-- Indeks untuk tabel `tb_mutasi`
--
ALTER TABLE `tb_mutasi`
  ADD PRIMARY KEY (`id_mutasi`);

--
-- Indeks untuk tabel `tb_pengapusan`
--
ALTER TABLE `tb_pengapusan`
  ADD PRIMARY KEY (`id_pengapusan`) USING BTREE;

--
-- Indeks untuk tabel `tb_perbaikan`
--
ALTER TABLE `tb_perbaikan`
  ADD PRIMARY KEY (`id_perbaikan`);

--
-- Indeks untuk tabel `tb_recon`
--
ALTER TABLE `tb_recon`
  ADD PRIMARY KEY (`id_recon`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `gedung`
--
ALTER TABLE `gedung`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `master_barang`
--
ALTER TABLE `master_barang`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `tbl_ruangan`
--
ALTER TABLE `tbl_ruangan`
  MODIFY `id_ruangan` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `kode_user` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_barang_perbaikan`
--
ALTER TABLE `tb_barang_perbaikan`
  MODIFY `id_barang_perbaikan` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tb_keputusan`
--
ALTER TABLE `tb_keputusan`
  MODIFY `id_keputusan` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tb_kib`
--
ALTER TABLE `tb_kib`
  MODIFY `id_kib` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_kir`
--
ALTER TABLE `tb_kir`
  MODIFY `id_kir` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT untuk tabel `tb_mutasi`
--
ALTER TABLE `tb_mutasi`
  MODIFY `id_mutasi` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `tb_pengapusan`
--
ALTER TABLE `tb_pengapusan`
  MODIFY `id_pengapusan` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tb_perbaikan`
--
ALTER TABLE `tb_perbaikan`
  MODIFY `id_perbaikan` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_recon`
--
ALTER TABLE `tb_recon`
  MODIFY `id_recon` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
