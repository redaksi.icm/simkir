<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->userdata('kode_user'))) {
            redirect(base_url('Login'));
        }
    }

    public function index()
    {
        $data['user'] = $this->Model_login->view_user_tampilan();
        $this->load->view('user/F_User/User_view', $data);
    }

    public function simpan_user()
    {
        $data = array(
            'nip_user' => $this->input->post('nip'),
            'nama_user' => $this->input->post('nama'),
            'jabatan' => $this->input->post('pilih_user'),
            'password' => (md5($this->input->post('pass'))),
        );

        $insert = $this->Model_login->simpan_user($data, 'tbl_user');
        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function hapus_User($kode)
    {

        $where = array('kode_user' => $kode);
        $data = $this->Model_login->hapus_user($where, 'tbl_user');

        if ($where == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function simpan_edit_user()
    {
        $kode = $this->input->post('kode_user_edit');
        $data = array(
            'nip_user' => $this->input->post('nip_edit'),
            'nama_user' => $this->input->post('nama_edit'),
            'jabatan' => $this->input->post('pilih_user_edit'),
            'password' => (md5($this->input->post('pass_edit'))),
        );

        $insert = $this->Model_login->simpan_edit_user($kode, $data);
        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }
}
