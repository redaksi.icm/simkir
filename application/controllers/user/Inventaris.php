<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Inventaris extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->userdata('kode_user'))) {
            redirect(base_url('Login'));
        }
    }

    public function index()
    {

        $this->load->view('user/F_inventaris/inventaris_view');
    }

    public function add_barang($kode)
    {
        $data['barang_kir_view'] = $this->Model_inventaris->barang_kir_view($kode);
        $data['view_add_barang_ruang'] = $this->Model_inventaris->view_ruang_add_barang($kode);
        $this->load->view('user/F_inventaris/add_barang_invetaris_view', $data);
        // echo json_encode($data);

    }

    public function view_add_barang()
    {
        $data = $this->Model_barang->view_barang();
        echo json_encode($data);
    }

    public function simpan_barang_kir()
    {
        $data = array(
            "id" => $this->input->post('pilih_kode_barang'),
            "jumlah_register" => $this->input->post('jumlah_regis'),
            "keterangan" => $this->input->post('keterangan'),
            "id_ruangan" => $this->input->post('kode_ruangan'),
            "recon" => 0,
        );

        $insert = $this->Model_inventaris->barang_kir($data, 'tb_kir');
        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function view_data_barang_kir()
    {
        $data['barang_kir_view'] = $this->Model_inventaris->barang_kir_view();
        $this->load->view('user/F_inventaris/add_barang_invetaris_view', $data);

        // echo json_encode($data);
    }

    public function simpan_recon_kir()
    {
        $kode = $this->input->post('id_kir_recon');
        $data2 = array(
            "recon" => date('Y')
        );
        $data = array(
            "id_kir" => $this->input->post('id_kir_recon'),
            "kondisi_baik" => $this->input->post('kondisik_baik_recon'),
            "kondisi_kurang_baik" => $this->input->post('kondisi_kurang_baik_recon'),
            "kondisi_rusak" => $this->input->post('kondisi_rusak_recon'),
            "keterangan" => $this->input->post('keterangan_recon'),
            "tahun_recon" => $this->input->post('tahunan_recon'),
        );
        $insert = $this->Model_inventaris->recon_kir($data, 'tb_recon');
        $insert2 = $this->Model_inventaris->simpan_update_kir($kode, $data2);
        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function view_data_recon_kirkan($kode)
    {
        $data['view_recon_kir_kir'] = $this->Model_inventaris->view_recon_kir_kir($kode);
        echo json_encode($data);
    }

    public function print_kir($tahun, $kode)
    {
        ob_start();
        $data['ruangan'] = $this->Model_inventaris->view_ruang_add_barang($kode);
        $data['data_barang'] = $this->Model_inventaris->viewkan_kir_barang($kode, $tahun);
        $this->load->view('user/F_inventaris/print_kir', $data);
        $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
        $pdf = new HTML2PDF('l', 'A4', 'en');
        $pdf->WriteHTML($html);
        $pdf->Output('Laporan KIR.pdf', 'D');
    }

    public function hapus_Inventaris($kode)
    {
        $where = array('id_kir' => $kode);
        $data = $this->Model_inventaris->hapus_Inventaris($where, 'tb_kir');

        if ($where == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }


    public function hapus_data_recon_tahunan($kode)
    {

        $where = array('id_recon' => $kode);
        $data = $this->Model_inventaris->hapus_data_recon_tahunan($where, 'tb_recon');

        if ($where == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function simpan_edit_recon_kir()
    {
        $kode = $this->input->post('kode_kir_edit');

        $data = array(
            "jumlah_register" => $this->input->post('jumlah_regis_edit'),
            "keterangan" => $this->input->post('keterangan_edit'),
        );
        $insert = $this->Model_inventaris->simpan_update_kir2($kode, $data);

        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }
}
