<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->userdata('kode_user'))) {
            redirect(base_url('Login'));
        }
    }

    public function index()
    {
        $this->load->view('user/home_view');
    }
}
