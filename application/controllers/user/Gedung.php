<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gedung extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->userdata('kode_user'))) {
            redirect(base_url('Login'));
        }
    }

    public function index()
    {
        $data['gedung'] = $this->Model_gedung->view_gedung();
        $this->load->view('user/F_gedung/gedung_view', $data);
    }

    public function view_data_gedung()
    {

        $data['gedung'] = $this->Model_gedung->view_gedung();
        echo json_encode($data);
    }

    public function simpan_gedung()
    {
        $data = array(
            'nama' => $this->input->post('nama_gedung'),
            'alamat' => $this->input->post('alamat_gedung'),
            'kode_lokasi' => $this->input->post('lokasi_gedung'),
        );

        $insert = $this->Model_gedung->simpan_gedung($data, 'gedung');
        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function view_edit($kode)
    {

        $data['view_edit_gedung'] = $this->Model_gedung->view_edit_gedung($kode);
        echo json_encode($data);
    }

    public function simpan_edit_gedung()
    {
        $kode = $this->input->post('edit_id_gedung');
        $data = array(
            'nama' => $this->input->post('edit_nama_gedung'),
            'alamat' => $this->input->post('edit_alamat_gedung'),
            'kode_lokasi' => $this->input->post('edit_lokasi_gedung'),
        );
        $insert = $this->Model_gedung->simpan_update_gedung($kode, $data);
        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function hapus_gedung($kode)
    {
        $where = array('id' => $kode);
        $data = $this->Model_gedung->hapus_gedung($where, 'gedung');

        if ($where == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }
}
