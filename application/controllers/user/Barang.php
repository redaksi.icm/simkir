<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->userdata('kode_user'))) {
            redirect(base_url('Login'));
        }
    }

    public function index()
    {
        $data['barang'] = $this->Model_barang->view_barang();

        $this->load->view('user/F_barang/barang_view',$data);
    }

    public function view_data_barang()
    {
        $data['barang'] = $this->Model_barang->view_barang();
        echo json_encode($data);
    }

    public function simpan_barang()
    {
        $data = array(
            "kode_barang" => $this->input->post('kode_barang'),
            "nama" => $this->input->post('nama_barang'),
            "merek" => $this->input->post('merek_barang'),
            "no_seri" => $this->input->post('no_seri'),
            "ukuran" => $this->input->post('ukuran'),
            "bahan" => $this->input->post('bahan'),
            "thn_pembelian" => $this->input->post('tahun_pembelian'),
            "harga" => $this->input->post('harga_barang'),
        );

        $insert = $this->Model_barang->simpan_barang($data, 'master_barang');
        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
        // echo json_encode($data);
    }

    public function view_edit_barang($kode)
    {

        $data['view_edit_barang'] = $this->Model_barang->view_edit_barang($kode);
        echo json_encode($data);
    }

    public function simpan_edit_barang()
    {
        $kode = $this->input->post('id_barang_edit');
        $data = array(
            "kode_barang" => $this->input->post('kode_barang_edit'),
            "nama" => $this->input->post('nama_barang_edit'),
            "merek" => $this->input->post('merek_barang_edit'),
            "no_seri" => $this->input->post('no_seri_edit'),
            "ukuran" => $this->input->post('ukuran_edit'),
            "bahan" => $this->input->post('bahan_edit'),
            "thn_pembelian" => $this->input->post('tahun_pembelian_edit'),
            "harga" => $this->input->post('harga_barang_edit'),
        );
        $insert = $this->Model_barang->simpan_update_barang($kode, $data);
        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function hapus_barang($kode)
    {
        $where = array('id' => $kode);
        $data = $this->Model_barang->hapus_barang($where, 'master_barang');

        if ($where == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }
}
