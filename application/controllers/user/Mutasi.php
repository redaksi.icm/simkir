<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mutasi extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->userdata('kode_user'))) {
            redirect(base_url('Login'));
        }
    }

    public function index()
    {
        $data['kib'] = $this->Modal_kibs->view_kibss();
        $this->load->view('user/F_Mutasi/mutasi_barang_view', $data);
    }

    public function simpan_berita_mutasi()
    {
        $data = array(
            "nomor" => $this->input->post('nomor'),
            "daftar" => $this->input->post('daftar'),
            "tanggal" => $this->input->post('tanggal'),
            "status" => 'Lihat'
        );

        $insert = $this->Modal_kibs->insert_kib_berita($data, 'tb_kib');

        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function add_data_mutasi($kode)
    {

        $data['mutasi'] = $this->Modal_kibs->view_mutasi_barang($kode);
        $data['barang'] = $this->Modal_kibs->view_barang_nya_kib3();
        $data['ruangan_mutasikan'] = $this->Modal_kibs->ruangan_mutasikan();
        $data['view_kib'] = $this->Modal_kibs->view_kib($kode);
        $this->load->view('user/F_Mutasi/add_data_mutasi', $data);
        // echo json_encode($data);
    }

    public function simpan_barang_mutasi()
    {

        $data = array(
            "id_kir" => $this->input->post('id_barang'),
            "id_kib" => $this->input->post('id_kibkib'),
            "mutasi_ke" => $this->input->post('ruangan_mutasi')
        );

        $insert = $this->Modal_kibs->insert_barang_mutasi($data, 'tb_mutasi');

        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function simpan_mutasi()
    {
        $kode = $this->input->post('id_kibkib');

        $data = array(
            "status" => 'Simpan',
        );

        $insert = $this->Modal_kibs->insert_mutasi($data, $kode);

        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function view_data_mutasi($kode)
    {
        $data['mutasi'] = $this->Modal_kibs->view_mutasi_barang($kode);
        $data['barang_mutasikan'] = $this->Modal_kibs->view_barang_nya_kib();
        $data['view_kib'] = $this->Modal_kibs->view_kib($kode);
        // echo json_encode($data);
        $this->load->view('user/F_Mutasi/view_data_mutasi', $data);
    }

    public function hapus_barang_mutasi($kode)
    {
        $where = array('id_mutasi' => $kode);
        $data = $this->Modal_kibs->hapus_barang_mutasi($where, 'tb_mutasi');

        if ($where == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function hapus_mutasicoy($kode)
    {
        $where = array('id_kib' => $kode);
        $data = $this->Modal_kibs->hapusnya_mutasi($where, 'tb_kib');

        if ($where == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function print_kib()
    {
        ob_start();
        $data['kib'] = $this->Modal_kibs->view_kibss();
        $this->load->view('user/F_Mutasi/print_kib', $data);
        $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
        $pdf = new HTML2PDF('l', 'A4', 'en');
        $pdf->WriteHTML($html);
        $pdf->Output('Laporan KIB.pdf', 'D');
    }

    public function print_mutasi_kib($kode)
    {
        ob_start();
        $data['mutasi'] = $this->Modal_kibs->view_mutasi_barang($kode);
        $data['view_kib'] = $this->Modal_kibs->view_kib($kode);
        $this->load->view('user/F_Mutasi/print_mutasi_kib', $data);

        $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
        $pdf = new HTML2PDF('l', 'A4', 'en');
        $pdf->WriteHTML($html);
        $pdf->Output('Laporan Mutasi KIB.pdf', 'D');
    }
}
