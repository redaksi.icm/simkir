<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penghapusan extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->userdata('kode_user'))) {
            redirect(base_url('Login'));
        }
    }

    public function index()
    {
        $data['keputusan'] = $this->Modal_keputusan->view_kepututsan();
        $this->load->view('user/F_penghapusan/penghapusan', $data);
    }

    public function simpan_berita_Keputusan()
    {
        $data = array(
            "nomor_keputusan" => $this->input->post('nomor'),
            "tentang" => $this->input->post('tetang'),
            "tanggal_keputusan" => $this->input->post('tanggal'),
            "status" => 'Lihat'
        );

        $insert = $this->Modal_keputusan->insert_keputusan_berita($data, 'tb_keputusan');

        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public  function add_data_penghapusan($kode)
    {
        $data['penga'] = $this->Modal_keputusan->view_pengapusan_barang($kode);
        $data['barang_hapus'] = $this->Modal_kibs->view_barang_nya_kib();
        $data['view_keputusan'] = $this->Modal_keputusan->view_keputusan($kode);
        $this->load->view('user/F_penghapusan/add_data_pengapusan', $data);

        // echo json_encode($data);
    }

    public function simpan_barang_penghapusan()
    {
        $data = array(
            "id" => $this->input->post('id_barang'),
            "id_keputusan" => $this->input->post('id_keputusan'),
            "nilai" => $this->input->post('nilai'),
            "akumulasi_penyusutan" => $this->input->post('akumulasi'),
            "nilai_buku" => $this->input->post('nilai_buku'),
        );

        $insert = $this->Modal_keputusan->insert_barang_hapus($data, 'tb_pengapusan');

        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function simpan_penghapusan()
    {
        $kode = $this->input->post('id_keputusan');

        $data = array(
            "status" => 'Simpan',
        );

        $insert = $this->Modal_keputusan->update_hapus($data, $kode);

        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }


    public function view_data_penghapusan($kode)
    {
        $data['penga'] = $this->Modal_keputusan->view_pengapusan_barang($kode);
        $data['barang_hapus'] = $this->Modal_kibs->view_barang_nya_kib();
        $data['view_keputusan'] = $this->Modal_keputusan->view_keputusan($kode);
        $this->load->view('user/F_penghapusan/view_penghapusan', $data);
    }

    public function hapus_keputusan_Penghapusan($kode)
    {
        $where = array('id_keputusan' => $kode);
        $data = $this->Modal_keputusan->hapusnya_keputusan($where, 'tb_keputusan');

        if ($where == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function hapus_penghapusancoy($kode)
    {
        $where = array('id_pengapusan' => $kode);
        $data = $this->Modal_keputusan->hapusnyatb_pengapusan($where, 'tb_pengapusan');

        if ($where == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }


    public function print_Penghapusan($kode)
    {
        ob_start();

        $data['penga'] = $this->Modal_keputusan->view_pengapusan_barang($kode);
        $data['barang_hapus'] = $this->Modal_kibs->view_barang_nya_kib();
        $data['view_keputusan'] = $this->Modal_keputusan->view_keputusan($kode);
        $this->load->view('user/F_penghapusan/print_penghapusan', $data);

        $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
        $pdf = new HTML2PDF('l', 'A4', 'en');
        $pdf->WriteHTML($html);
        $pdf->Output('Laporan Penghapusan Barang.pdf', 'D');
    }
}
