<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_kir extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->userdata('kode_user'))) {
            redirect(base_url('Login'));
        }
    }

    public function index()
    {
        $data['data_gedung'] = $this->Model_ruang->view_ruang();
        $this->load->view('user/F_inventaris/laporan_kir', $data);
        // echo json_encode($data);

    }

    public function viewkan_kir($kode, $tahun, $kondisi, $kode_barang)
    {

        if ($kode_barang == 0 || $kode_barang == null || empty($kode_barang)) {

            if ($kondisi == 'semua') {
                $data['data_gedung'] = $this->Model_inventaris->viewkan_kir_barang($kode, $tahun);
            } elseif ($kondisi == 'baik') {
                $data['data_gedung'] = $this->Model_inventaris->viewkan_kir_barang_baik($kode, $tahun);
            } elseif ($kondisi == 'kurang') {
                $data['data_gedung'] = $this->Model_inventaris->viewkan_kir_barang_kurang_baik($kode, $tahun);
            } elseif ($kondisi == 'rusak') {
                $data['data_gedung'] = $this->Model_inventaris->viewkan_kir_barang_rusak($kode, $tahun);
            }

            echo json_encode($data);
        } else {

            if ($kondisi == 'semua') {
                $data['data_gedung'] = $this->Model_inventaris->viewkan_kir_barang_kode($kode, $tahun, $kode_barang);
            } elseif ($kondisi == 'baik') {
                $data['data_gedung'] = $this->Model_inventaris->viewkan_kir_barang_baik_kode($kode, $tahun, $kode_barang);
            } elseif ($kondisi == 'kurang') {
                $data['data_gedung'] = $this->Model_inventaris->viewkan_kir_barang_kurang_baik_kode($kode, $tahun, $kode_barang);
            } elseif ($kondisi == 'rusak') {
                $data['data_gedung'] = $this->Model_inventaris->viewkan_kir_barang_rusak_kode($kode, $tahun, $kode_barang);
            }

            echo json_encode($data);
        }
    }
}
