<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ruang extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->userdata('kode_user'))) {
            redirect(base_url('Login'));
        }
    }

    public function index()
    {
        $data['ruangan'] = $this->Model_ruang->view_ruang();
        $this->load->view('user/F_ruang/ruang_view', $data);
    }

    public function view_data_ruangan()
    {
        $data['ruangan'] = $this->Model_ruang->view_ruang();
        echo json_encode($data);
    }

    public function view_gedung_ruangan()
    {
        $data = $this->Model_gedung->view_gedung();
        echo json_encode($data);
    }

    public function simpan_ruangan()
    {
        $data = array(
            'id' => $this->input->post('pilih_gedung'),
            'nama_ruangan' => $this->input->post('nama_ruangan'),
        );

        $insert = $this->Model_ruang->simpan_ruangansoy($data, 'tbl_ruangan');
        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function view_edit_ruang($kode)
    {
        $data['view_edit_ruang'] = $this->Model_ruang->view_edit_ruang($kode);
        echo json_encode($data);
    }

    public function simpan_edit_ruangan()
    {
        $kode = $this->input->post('id_ruangan_edit');
        $data = array(
            'id' => $this->input->post('pilih_gedung_edit'),
            'nama_ruangan' => $this->input->post('nama_ruangan_edit'),
        );
        $insert = $this->Model_ruang->simpan_update_ruang($kode, $data);
        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function hapus_ruangan($kode)
    {
        $where = array('id_ruangan' => $kode);
        $data = $this->Model_ruang->hapus_ruangan($where, 'tbl_ruangan');

        if ($where == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }
}
