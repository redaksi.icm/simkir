<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perbaikan extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->userdata('kode_user'))) {
            redirect(base_url('Login'));
        }
    }

    public function index()
    {
        $data['perbaikan'] = $this->Modal_keputusan->surat_perbaikan();
        $this->load->view('user/F_perbaikan/view_surat_perbaikan', $data);
    }

    public function simpan_perbaikan()
    {
        $data = array(
            "nomor_perbaikan" => $this->input->post('nomor'),
            "tentang" => $this->input->post('tetang'),
            "tanggal" => $this->input->post('tanggal'),
            "status" => 'Lihat'
        );

        $insert = $this->Modal_keputusan->simpan_surat_perbaikan($data, 'tb_perbaikan');

        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function add_perbiakan($kode)
    {
        $data['perbaikan'] = $this->Modal_keputusan->view_perbaikan_barang($kode);
        $data['ruangan'] = $this->Modal_keputusan->view_perbaikan_ruangan();
        $data['view_barang_perbaikan'] = $this->Modal_keputusan->view_barang_perbaikan($kode);
        $data['barang_add'] = $this->Modal_kibs->view_barang_nya_kib2();
        $this->load->view('user/F_perbaikan/tambah_barang_perbaikan', $data);

        // echo json_encode($data);
    }

    public function simpan_barang_perbaikan()
    {
        $data = array(
            "id" => $this->input->post('id_barang'),
            "id_perbaikan" => $this->input->post('id_perbaikan'),
            "id_ruangan" => $this->input->post('kode_ruangan'),
            "ket" => $this->input->post('keterangan'),
            "jumlah" => $this->input->post('jumlah')
        );

        $insert = $this->Modal_keputusan->simpan_barang_perbaikan($data, 'tb_barang_perbaikan');

        if ($data == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function hapus_Perbaikancoy($kode)
    {
        $where = array('id_perbaikan' => $kode);
        $data = $this->Modal_keputusan->hapusnyatb_perbaikan($where, 'tb_perbaikan');

        if ($where == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function hapus_barangcoy($kode)
    {
        $where = array('id_barang_perbaikan' => $kode);
        $data = $this->Modal_keputusan->hapusnyatb_barang_perbaikan($where, 'tb_barang_perbaikan');

        if ($where == TRUE) {
            echo 'sukses';
        } else {
            echo 'error';
        }
    }

    public function print_Perbaikan($kode)
    {
        ob_start();

        $data['perbaikan'] = $this->Modal_keputusan->view_perbaikan_barang($kode);
        $data['ruangan'] = $this->Modal_keputusan->view_perbaikan_ruangan();
        $data['view_barang_perbaikan'] = $this->Modal_keputusan->view_barang_perbaikan($kode);
        $data['barang_add'] = $this->Modal_kibs->view_barang_nya_kib2();
        $this->load->view('user/F_perbaikan/print_barang_perbaikan', $data);


        $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/html2pdf/html2pdf.class.php');
        $pdf = new HTML2PDF('l', 'A4', 'en');
        $pdf->WriteHTML($html);
        $pdf->Output('Laporan Perbaikan Barang.pdf', 'D');
    }
}
