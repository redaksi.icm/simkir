<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('index');
	}

	public function aksi_login()
	{
		$username = $this->input->post('username_nip');
		$password = md5($this->input->post('password'));

		$where = array(
			'nip_user' =>  $username,
			'password' => $password
		);

		$cek = $this->Model_login->cek_login("tbl_user", $where)->num_rows();
		$data_user = $this->Model_login->view_user($username, $password);

		if ($cek > 0) {
			$data_session = array(
				'nama_user' => $data_user->nama_user,
				'jabatan' => $data_user->jabatan,
				'nip_user' => $data_user->nip_user,
				'kode_user' => $data_user->kode_user
			);

			// echo json_encode($data_session);
			$this->session->set_userdata($data_session);
			redirect(base_url("user/Home"));
		} else {

			// $this->session->set_flashdata('success','Berhasil');
			// redirect(base_url("Login"));

			$this->session->set_flashdata('gagal', 'error_login');
			// $this->load->view('index', $data);
			redirect('/');
		}
	}

	public function keluar()
	{

		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
}
