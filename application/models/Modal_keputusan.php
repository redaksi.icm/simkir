<?php

/**
 * 
 */
class Modal_keputusan extends CI_Model
{

    function view_kepututsan()
    {
        $data = $this->db->query("SELECT * from tb_keputusan order by id_keputusan desc");
        return $data->result();
    }

    function insert_keputusan_berita($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function view_keputusan($kode)
    {
        $data = $this->db->query("SELECT * from tb_keputusan where id_keputusan='$kode'");
        return $data->row();
    }

    function insert_barang_hapus($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function view_pengapusan_barang($kode)
    {
        $data = $this->db->query("SELECT * 
        from tb_pengapusan
        join master_barang on master_barang.id=tb_pengapusan.id
        join tb_kir on tb_kir.id=tb_pengapusan.id
        where id_keputusan='$kode'");
        return $data->result();
    }

    function update_hapus($data, $kode)
    {
        $this->db->where('id_keputusan', $kode);
        return $this->db->update('tb_keputusan', $data);
    }

    function surat_perbaikan()
    {
        $data = $this->db->query("SELECT * from tb_perbaikan order by id_perbaikan desc");
        return $data->result();
    }

    function simpan_surat_perbaikan($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function view_perbaikan_barang($kode)
    {
        $data = $this->db->query("SELECT * from tb_perbaikan where id_perbaikan='$kode'");
        return $data->row();
    }

    public function simpan_barang_perbaikan($data, $table)
    {
        $this->db->insert($table, $data);
    }

    public function view_barang_perbaikan($kode)
    {
        $data = $this->db->query("SELECT * 
        from  tb_barang_perbaikan
        inner join master_barang on master_barang.id=tb_barang_perbaikan.id
        inner join tbl_ruangan on tbl_ruangan.id_ruangan=tb_barang_perbaikan.id_ruangan
        -- inner join tb_kir on tb_kir.id=tb_barang_perbaikan.id
        -- inner join tb_recon on tb_recon.id_kir=tb_kir.id_kir
        -- inner join tbl_ruangan on tbl_ruangan.id_ruangan=tb_kir.id_ruangan
        -- where id_perbaikan='$kode'
        ");
        return $data->result();
    }

    function view_perbaikan_ruangan()
    {
        $data = $this->db->query("SELECT *
        from tbl_ruangan");
        return $data->result();
    }

    function hapusnya_keputusan($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function hapusnyatb_pengapusan($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function hapusnyatb_perbaikan($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function hapusnyatb_barang_perbaikan($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
