<?php

/**
 * 
 */
class Model_login extends CI_Model
{

    function cek_login($table, $where)
    {
        return $this->db->get_where($table, $where);
    }

    public function view_user($username, $password)
    {
        $data = $this->db->query("SELECT * from tbl_user where nip_user ='$username' and password = '$password'");
        return $data->row();
    }

    public function view_user_tampilan()
    {
        $data = $this->db->query("SELECT * from tbl_user");
        return $data->result();
    }

    function simpan_user($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function hapus_user($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function simpan_edit_user($kode, $data)
    {
        $this->db->where('kode_user', $kode);
        return $this->db->update('tbl_user', $data);
    }
}
