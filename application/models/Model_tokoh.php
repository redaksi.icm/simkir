<?php

/**
 * 
 */
class Model_tokoh extends CI_Model
{

    function view_barang()
    {
        $data = $this->db->query("SELECT * from master_barang order by id desc");
        return $data->result();
    }

    function simpan_barang($data, $table)
    {

        $this->db->insert($table, $data);
    }

    function view_edit_barang($kode)
    {
        $data = $this->db->query("SELECT * from master_barang where id='$kode'");
        return $data->row();
    }

    function simpan_update_barang($kode, $data)
    {
        $this->db->where('id', $kode);
        return $this->db->update('master_barang', $data);
    }

    function hapus_barang($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
