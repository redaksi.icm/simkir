<?php

/**
 * 
 */
class Model_inventaris extends CI_Model
{

    function view_ruang_add_barang($kode)
    {
        $data = $this->db->query("SELECT * from tbl_ruangan inner join gedung on tbl_ruangan.id=gedung.id where tbl_ruangan.id_ruangan='$kode'");
        return $data->row();
    }

    function barang_kir($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function barang_kir_view($kode)
    {
        $data = $this->db->query("SELECT 
        tb_kir.id_kir,
        tb_kir.id_ruangan,
        tb_kir.jumlah_register,
        tb_kir.keterangan,
        master_barang.kode_barang,
        master_barang.nama,
        master_barang.harga,
        master_barang.thn_pembelian
        from tb_kir 
        inner join master_barang on master_barang.id = tb_kir.id
        where tb_kir.id_ruangan = $kode
        order by tb_kir.id_kir desc
        ");
        return $data->result();
    }
    function recon_kir($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function view_recon_kir_kir($kode)
    {
        $data =  $this->db->query("SELECT * FROM tb_recon where id_kir = '$kode' order by tahun_recon desc");
        return $data->result();
    }

    function viewkan_kir_barang($kode, $tahun)
    {
        $data =  $this->db->query("SELECT 
        tb_recon.id_recon, 
        tb_recon.id_kir, 
        tb_recon.kondisi_baik, 
        tb_recon.kondisi_kurang_baik, 
        tb_recon.kondisi_rusak, 
        tb_recon.keterangan, 
        tb_kir.id,
        tb_kir.jumlah_register,
        tb_kir.id_ruangan,
        master_barang.kode_barang,
        master_barang.nama,
        master_barang.harga,
        master_barang.thn_pembelian,
        master_barang.merek,
        master_barang.no_seri,
        master_barang.ukuran,
        master_barang.bahan,
        master_barang.thn_pembelian
        FROM tb_recon
        inner join tb_kir on tb_kir.id_kir=tb_recon.id_kir
        inner join master_barang on master_barang.id=tb_kir.id
        inner join tbl_ruangan on tbl_ruangan.id_ruangan=tb_kir.id_ruangan
        where tb_kir.id_ruangan = '$kode' and tahun_recon ='$tahun' order by tahun_recon desc");
        return $data->result();
    }

    function simpan_update_kir($kode, $data)
    {
        $this->db->where('id_kir', $kode);
        return $this->db->update('tb_kir', $data);
    }

    function hapus_Inventaris($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function hapus_data_recon_tahunan($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function simpan_update_kir2($kode, $data)
    {
        $this->db->where('id_kir', $kode);
        return $this->db->update('tb_kir', $data);
    }

    function viewkan_kir_barang_baik($kode, $tahun)
    {
        $data =  $this->db->query("SELECT 
        tb_recon.id_recon, 
        tb_recon.id_kir, 
        tb_recon.kondisi_baik, 
        tb_recon.kondisi_kurang_baik, 
        tb_recon.kondisi_rusak, 
        tb_recon.keterangan, 
        tb_kir.id,
        tb_kir.jumlah_register,
        tb_kir.id_ruangan,
        master_barang.kode_barang,
        master_barang.nama,
        master_barang.harga,
        master_barang.thn_pembelian,
        master_barang.merek,
        master_barang.no_seri,
        master_barang.ukuran,
        master_barang.bahan,
        master_barang.thn_pembelian
        FROM tb_recon
        inner join tb_kir on tb_kir.id_kir=tb_recon.id_kir
        inner join master_barang on master_barang.id=tb_kir.id
        inner join tbl_ruangan on tbl_ruangan.id_ruangan=tb_kir.id_ruangan
        where tb_kir.id_ruangan = '$kode' and tahun_recon ='$tahun' and kondisi_baik >= 1  order by tahun_recon desc");
        return $data->result();
    }

    function viewkan_kir_barang_kurang_baik($kode, $tahun)
    {
        $data =  $this->db->query("SELECT 
        tb_recon.id_recon, 
        tb_recon.id_kir, 
        tb_recon.kondisi_baik, 
        tb_recon.kondisi_kurang_baik, 
        tb_recon.kondisi_rusak, 
        tb_recon.keterangan, 
        tb_kir.id,
        tb_kir.jumlah_register,
        tb_kir.id_ruangan,
        master_barang.kode_barang,
        master_barang.nama,
        master_barang.harga,
        master_barang.thn_pembelian,
        master_barang.merek,
        master_barang.no_seri,
        master_barang.ukuran,
        master_barang.bahan,
        master_barang.thn_pembelian
        FROM tb_recon
        inner join tb_kir on tb_kir.id_kir=tb_recon.id_kir
        inner join master_barang on master_barang.id=tb_kir.id
        inner join tbl_ruangan on tbl_ruangan.id_ruangan=tb_kir.id_ruangan
        where tb_kir.id_ruangan = '$kode' and tahun_recon ='$tahun' and kondisi_kurang_baik >= 1  order by tahun_recon desc");
        return $data->result();
    }

    function viewkan_kir_barang_rusak($kode, $tahun)
    {
        $data =  $this->db->query("SELECT 
        tb_recon.id_recon, 
        tb_recon.id_kir, 
        tb_recon.kondisi_baik, 
        tb_recon.kondisi_kurang_baik, 
        tb_recon.kondisi_rusak, 
        tb_recon.keterangan, 
        tb_kir.id,
        tb_kir.jumlah_register,
        tb_kir.id_ruangan,
        master_barang.kode_barang,
        master_barang.nama,
        master_barang.harga,
        master_barang.thn_pembelian,
        master_barang.merek,
        master_barang.no_seri,
        master_barang.ukuran,
        master_barang.bahan,
        master_barang.thn_pembelian
        FROM tb_recon
        inner join tb_kir on tb_kir.id_kir=tb_recon.id_kir
        inner join master_barang on master_barang.id=tb_kir.id
        inner join tbl_ruangan on tbl_ruangan.id_ruangan=tb_kir.id_ruangan
        where tb_kir.id_ruangan = '$kode' and tahun_recon ='$tahun' and kondisi_rusak >= 1  order by tahun_recon desc");
        return $data->result();
    }


    // batas

    function viewkan_kir_barang_baik_kode($kode, $tahun, $kode_barang)
    {
        $data =  $this->db->query("SELECT 
        tb_recon.id_recon, 
        tb_recon.id_kir, 
        tb_recon.kondisi_baik, 
        tb_recon.kondisi_kurang_baik, 
        tb_recon.kondisi_rusak, 
        tb_recon.keterangan, 
        tb_kir.id,
        tb_kir.jumlah_register,
        tb_kir.id_ruangan,
        master_barang.kode_barang,
        master_barang.nama,
        master_barang.harga,
        master_barang.thn_pembelian,
        master_barang.merek,
        master_barang.no_seri,
        master_barang.ukuran,
        master_barang.bahan,
        master_barang.thn_pembelian
        FROM tb_recon
        inner join tb_kir on tb_kir.id_kir=tb_recon.id_kir
        inner join master_barang on master_barang.id=tb_kir.id
        inner join tbl_ruangan on tbl_ruangan.id_ruangan=tb_kir.id_ruangan
        where tb_kir.id_ruangan = '$kode' and tahun_recon ='$tahun' and kondisi_baik >= 1
        and master_barang.kode_barang = '$kode_barang' order by tahun_recon desc");
        return $data->result();
    }

    function viewkan_kir_barang_kurang_baik_kode($kode, $tahun, $kode_barang)
    {
        $data =  $this->db->query("SELECT 
        tb_recon.id_recon, 
        tb_recon.id_kir, 
        tb_recon.kondisi_baik, 
        tb_recon.kondisi_kurang_baik, 
        tb_recon.kondisi_rusak, 
        tb_recon.keterangan, 
        tb_kir.id,
        tb_kir.jumlah_register,
        tb_kir.id_ruangan,
        master_barang.kode_barang,
        master_barang.nama,
        master_barang.harga,
        master_barang.thn_pembelian,
        master_barang.merek,
        master_barang.no_seri,
        master_barang.ukuran,
        master_barang.bahan,
        master_barang.thn_pembelian
        FROM tb_recon
        inner join tb_kir on tb_kir.id_kir=tb_recon.id_kir
        inner join master_barang on master_barang.id=tb_kir.id
        inner join tbl_ruangan on tbl_ruangan.id_ruangan=tb_kir.id_ruangan
        where tb_kir.id_ruangan = '$kode' and tahun_recon ='$tahun' and kondisi_kurang_baik >= 1 
        and master_barang.kode_barang = '$kode_barang' order by tahun_recon desc");
        return $data->result();
    }

    function viewkan_kir_barang_rusak_kode($kode, $tahun, $kode_barang)
    {
        $data =  $this->db->query("SELECT 
        tb_recon.id_recon, 
        tb_recon.id_kir, 
        tb_recon.kondisi_baik, 
        tb_recon.kondisi_kurang_baik, 
        tb_recon.kondisi_rusak, 
        tb_recon.keterangan, 
        tb_kir.id,
        tb_kir.jumlah_register,
        tb_kir.id_ruangan,
        master_barang.kode_barang,
        master_barang.nama,
        master_barang.harga,
        master_barang.thn_pembelian,
        master_barang.merek,
        master_barang.no_seri,
        master_barang.ukuran,
        master_barang.bahan,
        master_barang.thn_pembelian
        FROM tb_recon
        inner join tb_kir on tb_kir.id_kir=tb_recon.id_kir
        inner join master_barang on master_barang.id=tb_kir.id
        inner join tbl_ruangan on tbl_ruangan.id_ruangan=tb_kir.id_ruangan
        where tb_kir.id_ruangan = '$kode' and tahun_recon ='$tahun' and kondisi_rusak >= 1 
        and master_barang.kode_barang = '$kode_barang'
         order by tahun_recon desc");
        return $data->result();
    }

    function viewkan_kir_barang_kode($kode, $tahun, $kode_barang)
    {
        $data =  $this->db->query("SELECT 
        tb_recon.id_recon, 
        tb_recon.id_kir, 
        tb_recon.kondisi_baik, 
        tb_recon.kondisi_kurang_baik, 
        tb_recon.kondisi_rusak, 
        tb_recon.keterangan, 
        tb_kir.id,
        tb_kir.jumlah_register,
        tb_kir.id_ruangan,
        master_barang.kode_barang,
        master_barang.nama,
        master_barang.harga,
        master_barang.thn_pembelian,
        master_barang.merek,
        master_barang.no_seri,
        master_barang.ukuran,
        master_barang.bahan,
        master_barang.thn_pembelian
        FROM tb_recon
        inner join tb_kir on tb_kir.id_kir=tb_recon.id_kir
        inner join master_barang on master_barang.id=tb_kir.id
        inner join tbl_ruangan on tbl_ruangan.id_ruangan=tb_kir.id_ruangan
        where tb_kir.id_ruangan = '$kode' and tahun_recon ='$tahun'
        and master_barang.kode_barang = '$kode_barang'
         order by tahun_recon desc");
        return $data->result();
    }
}
