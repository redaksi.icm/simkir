<?php

/**
 * 
 */
class Model_gedung extends CI_Model
{

    function view_gedung()
    {
        $data = $this->db->query("SELECT * from gedung order by id desc");
        return $data->result();
    }

    function simpan_gedung($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function view_edit_gedung($kode)
    {

        $data = $this->db->query("SELECT * from gedung where id='$kode'");
        return $data->row();
    }

    function simpan_update_gedung($kode, $data)
    {

        $this->db->where('id', $kode);
        return $this->db->update('gedung', $data);
    }

    function hapus_gedung($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
