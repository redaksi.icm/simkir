<?php

/**
 * 
 */
class Model_ruang extends CI_Model
{

    function view_ruang()
    {
        $data = $this->db->query("SELECT * from tbl_ruangan inner join gedung on tbl_ruangan.id=gedung.id order by tbl_ruangan.id_ruangan desc");
        return $data->result();
    }

    function simpan_ruangansoy($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function view_edit_ruang($kode)
    {
        $data = $this->db->query("SELECT * from tbl_ruangan where id_ruangan='$kode'");
        return $data->row();
    }
    function simpan_update_ruang($kode, $data)
    {
        $this->db->where('id_ruangan', $kode);
        return $this->db->update('tbl_ruangan', $data);
    }

    function hapus_ruangan($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
