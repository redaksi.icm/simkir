<?php

/**
 * 
 */
class Modal_kibs extends CI_Model
{

    function view_kibss()
    {
        $data = $this->db->query("SELECT * from tb_kib order by id_kib desc");
        return $data->result();
    }

    function insert_kib_berita($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function view_kib($kode)
    {
        $data = $this->db->query("SELECT * from tb_kib where id_kib='$kode'");
        return $data->row();
    }

    function view_barang_nya_kib()
    {
        $data = $this->db->query("SELECT tb_kir.id, tb_kir.id_ruangan, master_barang.nama, tbl_ruangan.nama_ruangan, master_barang.harga
        from tb_kir 
        inner join master_barang on master_barang.id=tb_kir.id
        inner join tbl_ruangan on tbl_ruangan.id_ruangan=tb_kir.id_ruangan");
        return $data->result();
    }

    function view_barang_nya_kib2()
    {
        $data = $this->db->query("SELECT *
        from master_barang");
        return $data->result();
    }

    function view_barang_nya_kib3()
    {
        $data = $this->db->query("SELECT tbl_ruangan.id_ruangan,nama_ruangan,tb_kir.id,nama,tb_kir.id_kir
        from tbl_ruangan
        join tb_kir on tbl_ruangan.id_ruangan=tb_kir.id_ruangan
        join master_barang on tb_kir.id=master_barang.id
        ");
        return $data->result();

        // $data = $this->db->query("SELECT tb_kir.id  FROM tb_kir
        // inner join master_barang on tb_kir.id=master_barang.id
        // ");
        // return $data->result();
    }

    function insert_barang_mutasi($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function view_mutasi_barang($kode)
    {
        $data = $this->db->query("SELECT *
        from tb_mutasi
        join tb_kir on tb_mutasi.id_kir=tb_kir.id_kir
        join tbl_ruangan on tb_kir.id_ruangan=tbl_ruangan.id_ruangan
        join master_barang on tb_kir.id=master_barang.id
        where id_kib='$kode'");
        return $data->result();
    }

    function insert_mutasi($data, $kode)
    {
        $this->db->where('id_kib', $kode);
        return $this->db->update('tb_kib', $data);
    }

    function ruangan_mutasikan()
    {
        $data = $this->db->query("SELECT *
        from tbl_ruangan");
        return $data->result();
    }

    function hapus_barang_mutasi($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
    function hapusnya_mutasi($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
