<?php $this->load->view('user/layer/header') ?>
<!-- tempat css/javascript -->
<style>
    /* kosong */
</style>
<?php $this->load->view('user/layer/body_atas') ?>

<div id="page_content_inner">
    <h3 class="heading_b uk-margin-bottom">DASHBOARD > USER</h3>
    <div class="md-card">
        <div class="uk-width-medium-1-6" style="padding-left: 890px;">
            <a class="md-btn md-btn-primary" href="#" style="width: max-content;" title="Tambah user" data-uk-modal="{target:'#tambah_user'}">Tambah User</a>
        </div>
        <div class="md-card-content">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="gedung_table" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>KODE USER</th>
                                <th>NIP</th>
                                <th>NAMA USER</th>
                                <th>JABATAN</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($user as $value) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $value->kode_user; ?></td>
                                    <td><?php echo $value->nip_user; ?></td>
                                    <td><?php echo $value->nama_user; ?></td>
                                    <td><?php echo $value->jabatan; ?></td>
                                    <td>
                                        <a href="#" id="edit_user" data-uk-modal="{target:'#edit_userkan'}"><i class="md-icon material-icons" style="color:green;">create</i></a>
                                        <a href="#" onclick="hapus_data('<?php echo $value->kode_user; ?>')"><i class="md-icon material-icons" style="color:red;">delete</i></a>


                                    </td>
                                </tr>

                            <?php } ?>
                        </tbody>
                    </table>
                    <div id="hasil"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- <div class="md-fab-wrapper">
    <a class="md-fab md-fab-success" href="#" data-uk-tooltip="{pos:'right'}" title="Tambah User" data-uk-modal="{target:'#tambah_user'}"><i class="material-icons">playlist_add</i></a>
</div> -->

<div class="uk-modal" id="tambah_user">
    <form action="" method="POST" id="input_user">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Tambah Data User</h3>
            </div>
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-2-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>NIP USER</label>
                                    <input type="text" name="nip" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>NAMA USER</label>
                                    <input type="text" name="nama" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>PASSWORD</label>
                                    <input type="text" name="pass" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <label>NAMA JABATAN</label><br>
                                    <select id="pilih_user" name="pilih_user" class="md-input pilih_user">
                                        <option value="0">Pilih Jabatan</option>
                                        <option value="admin">Admin</option>
                                        <option value="user">User</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close" onclick="input_user()">SIMPAN</button>
            </div>
        </div>
    </form>
</div>


<div class="uk-modal" id="edit_userkan">
    <form action="" method="POST" id="input_edit_user">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Edit Data User</h3>
            </div>
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-2-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>NIP USER</label>
                                    <input type="text" name="nip_edit" id="nip_edit" required class="md-input label-fixed" />
                                    <input type="hidden" name="kode_user_edit" id="kode_user_edit" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>NAMA USER</label>
                                    <input type="text" name="nama_edit" id="nama_edit" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <br>
                                    <label>NAMA JABATAN</label>
                                    <select id="pilih_user_edit" name="pilih_user_edit" class="md-input pilih_user_edit">
                                        <option value="0">Pilih Jabatan</option>
                                        <option value="admin">Admin</option>
                                        <option value="user">User</option>
                                    </select>
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>NEW PASSWORD</label>
                                    <input type="text" name="pass_edit" id="pass_edit" required class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close" onclick="input_edit_userkan()">SIMPAN</button>
            </div>
        </div>
    </form>
</div>

<?php $this->load->view('user/layer/body_bawah'); ?>
<script src="<?php echo base_url('/assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<!-- datatables colVis-->
<script src="<?php echo base_url('/assets/bower_components/datatables-colvis/js/dataTables.colVis.js') ?>"></script>
<!-- datatables tableTools-->
<script src="<?php echo base_url('/assets/bower_components/datatables-tabletools/js/dataTables.tableTools.js') ?>"></script>
<!-- datatables custom integration -->
<script src="<?php echo base_url('/assets/assets/js/custom/datatables_uikit.min.js') ?>"></script>

<!--  datatables functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/plugins_datatables.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/pages/components_notifications.min.js') ?>"></script>
<!-- tempat javascript -->
<script type="text/javascript">
    function input_user() {
        var data = $('#input_user').serialize();
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/User/simpan_user') ?>",
            data: data,
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/User"); ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/User"); ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);
    };


    $('#gedung_table tbody').on('click', '#edit_user', function() {
        var currow = $(this).closest('tr');
        var col1 = currow.find('td:eq(1)').text();
        var col2 = currow.find('td:eq(2)').text();
        var col3 = currow.find('td:eq(3)').text();
        var col4 = currow.find('td:eq(4)').text();
        // console.log(result);
        document.getElementById('kode_user_edit').value = col1;
        document.getElementById('nip_edit').value = col2;
        document.getElementById('nama_edit').value = col3;
        $('#pilih_user_edit').val(col4).change();

    });

    function input_edit_userkan() {
        var data = $('#input_edit_user').serialize();
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/User/simpan_edit_user') ?>",
            data: data,
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/User"); ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/User"); ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);
    };

    function hapus_data(kode) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url('/user/User/hapus_User/') ?>' + kode,
            success: function(data) {

                if (data == 'sukses') {
                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Dihapus...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/User"); ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Dihapus...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/User"); ?>');
                    }, 1000);
                }
                console.log(data);
            }

        })
        console.log(kode);
    }
</script>

<?php $this->load->view('user/layer/footer'); ?>