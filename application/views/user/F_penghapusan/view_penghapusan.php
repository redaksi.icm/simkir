<?php $this->load->view('user/layer/header') ?>
<!-- tempat css/javascript -->
<link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/kendo-ui/styles/kendo.common-material.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/kendo-ui/styles/kendo.material.min.css') ?>" />
<style>
    /* kosong */
</style>
<?php $this->load->view('user/layer/body_atas') ?>
<div id="page_content_inner">
    <h3 class="heading_b uk-margin-bottom">DASHBOARD > VIEW PENGHAPUSAN BARANG</h3>
    <div class="md-card">
        <h3 class="uk-text-center"></h3>
        <div class="md-card-content">
            <table id="" class="uk" cellspacing="0" style="width: 100%;max-width: 100%;font-size: 17px;">
                <tr>
                    <td style="width: 10%;">TENTANG</td>
                    <td style="width: 1%;">:</td>
                    <td>
                        <?php echo $view_keputusan->tentang; ?>
                        <input type="hidden" name="id_keputusan" id="id_keputusan" value="<?php echo $view_keputusan->id_keputusan; ?>">
                    </td>
                </tr>
                <tr>
                    <td>NOMOR</td>
                    <td>:</td>
                    <td><?php echo $view_keputusan->nomor_keputusan; ?></td>
                </tr>
                <tr>
                    <td>TANGGAL</td>
                    <td>:</td>
                    <td><?php echo $view_keputusan->tanggal_keputusan; ?></td>
                </tr>
            </table>
            <div class="uk-width-medium-1-6" style="padding-left: 850px;">
                <a class="md-btn md-btn-success" href="#" style="width: max-content;" title="Tambah Gedung" onclick="print_data_hapus()">Print PENGHAPUSAN</a>
            </div>
        </div>
    </div>

    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <table id="mutasi_table" class="uk-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Tahun Pembelian</th>
                        <th>Jumlah Register</th>
                        <th>Harga</th>
                        <th>Nilai</th>
                        <th>Akumulasi Penyusutan</th>
                        <th>Nilai Buku</th>
                        <th>Ket</th>
                    </tr>
                </thead>
                <?php
                $no = 1;
                foreach ($penga as $value_mutasi) {

                ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $value_mutasi->kode_barang; ?></td>
                        <td><?php echo $value_mutasi->nama; ?></td>
                        <td><?php echo $value_mutasi->thn_pembelian; ?></td>
                        <td><?php echo $value_mutasi->jumlah_register; ?></td>
                        <td><?php echo number_format($value_mutasi->harga, 2); ?></td>
                        <td><?php echo number_format($value_mutasi->nilai) ?></td>
                        <td><?php echo number_format($value_mutasi->akumulasi_penyusutan) ?></td>
                        <td><?php echo number_format($value_mutasi->nilai_buku) ?></td>
                        <td><?php echo $value_mutasi->keterangan; ?></td>
                    </tr>


                <?php } ?>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>



<?php $this->load->view('user/layer/body_bawah'); ?>
<script src="<?php echo base_url('/assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<!-- datatables colVis-->
<script src="<?php echo base_url('/assets/bower_components/datatables-colvis/js/dataTables.colVis.js') ?>"></script>
<!-- datatables tableTools-->
<script src="<?php echo base_url('/assets/bower_components/datatables-tabletools/js/dataTables.tableTools.js') ?>"></script>
<!-- datatables custom integration -->
<script src="<?php echo base_url('/assets/assets/js/custom/datatables_uikit.min.js') ?>"></script>

<!--  datatables functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/plugins_datatables.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/pages/components_notifications.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/kendoui_custom.min.js') ?>"></script>

<!--  kendoui functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/kendoui.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js') ?>"></script>
<!-- htmleditor (codeMirror) -->
<script src="<?php echo base_url('/assets/assets/js/uikit_htmleditor_custom.min.js'); ?>"></script>
<!-- inputmask-->
<script src="<?php echo base_url('/assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js') ?>"></script>

<!--  forms advanced functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/forms_advanced.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/bower_components/parsleyjs/dist/parsley.min.js') ?>"></script>
<!-- jquery steps -->
<script src="<?php echo base_url('/assets/assets/js/custom/wizard_steps.min.js') ?>"></script>

<!--  forms wizard functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/forms_wizard.min.js') ?>"></script>

<script>
    $('#mutasi_table').dataTable();

    function print_data_hapus() {
        var id_keputusan = $('#id_keputusan').val();
        url = "<?php echo base_url('user/Penghapusan/print_Penghapusan'); ?>";
        url = url + '/' + id_keputusan;
        var win = window.open(url, '_blank');
        win.focus();

    }
</script>

<?php $this->load->view('user/layer/footer'); ?>