<?php $this->load->view('user/layer/header') ?>
<!-- tempat css/javascript -->
<link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/kendo-ui/styles/kendo.common-material.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/kendo-ui/styles/kendo.material.min.css') ?>" />
<style>
    /* kosong */
</style>
<?php $this->load->view('user/layer/body_atas') ?>
<div id="page_content_inner">
    <h3 class="heading_b uk-margin-bottom">DASHBOARD > PENGHAPUSAN BARANG</h3>
    <div class="md-card">
        <h3 class="uk-text-center">Pilih Barang yang Akan Hapus</h3>
        <div class="md-card-content">
            <table id="" class="uk" cellspacing="0" style="width: 100%;max-width: 100%;font-size: 17px;">
                <tr>
                    <td style="width: 10%;">TENTANG</td>
                    <td style="width: 1%;">:</td>
                    <td>
                        <?php echo $view_keputusan->tentang; ?>
                        <input type="hidden" name="id_keputusan" id="id_keputusan" value="<?php echo $view_keputusan->id_keputusan; ?>">
                    </td>
                </tr>
                <tr>
                    <td>NOMOR</td>
                    <td>:</td>
                    <td><?php echo $view_keputusan->nomor_keputusan; ?></td>
                </tr>
                <tr>
                    <td>TANGGAL</td>
                    <td>:</td>
                    <td><?php echo $view_keputusan->tanggal_keputusan; ?></td>
                </tr>
            </table>
        </div>
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <label for="" style="color: red;">PILIH BARANG..</label>
                                    <select id="val_select" class="kode_barangg" required data-md-selectize style="width: 100%;" onchange="cari_harga()">
                                        <option value="0">Pilih...</option>
                                        <?php
                                        foreach ($barang_hapus as $value_brg) {
                                        ?>
                                            <option value="<?php echo $value_brg->id; ?>"><?php echo $value_brg->nama; ?>, (<?php echo $value_brg->nama_ruangan; ?>)</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>Harga Barang</label>
                                    <input type="text" name="harga_barangkan" id="harga_barangkan" required class="md-input label-fixed" />
                                    <!-- <input type="text" name="id_ruangan_kan" id="id_ruangan_kan" required class="md-input label-fixed" /> -->
                                </div>
                                <div class="uk-width-medium-1-2">
                                    <br>
                                    <a onclick="tambahkan_barang_hapus()" class="md-btn md-btn-danger" href="#">Tambah Barang</a>
                                </div>
                                <div class="uk-width-medium-1-2">
                                    <br>
                                    <a onclick="simpan_mutasi()" class="md-btn md-btn-danger" href="#">Simpan Penghapusan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <div class="uk-width-medium-2-2">
                                        <label>Nilai (Rp)</label>
                                        <input type="text" name="nilai" id="nilai" required class="md-input label-fixed" />
                                    </div>
                                    <div class="uk-width-medium-2-2">
                                        <br>
                                        <label>Akumulasi Penyusutan (Rp)</label>
                                        <input type="text" name="akumulasi" id="akumulasi" required class="md-input label-fixed" />
                                    </div>
                                    <div class="uk-width-medium-2-2">
                                        <br>
                                        <label>Nilai Buku (Rp)</label>
                                        <input type="text" name="nilai_buku" id="nilai_buku" required class="md-input label-fixed" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <table id="mutasi_table" class="uk-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Tahun Pembelian</th>
                        <th>Jumlah Register</th>
                        <th>Harga</th>
                        <th>Nilai</th>
                        <th>Akumulasi Penyusutan</th>
                        <th>Nilai Buku</th>
                        <th>Ket</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <?php
                $no = 1;
                foreach ($penga as $value_mutasi) {

                ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $value_mutasi->kode_barang; ?></td>
                        <td><?php echo $value_mutasi->nama; ?></td>
                        <td><?php echo $value_mutasi->thn_pembelian; ?></td>
                        <td><?php echo $value_mutasi->jumlah_register; ?></td>
                        <td><?php echo number_format($value_mutasi->harga, 2); ?></td>
                        <td><?php echo number_format($value_mutasi->nilai) ?></td>
                        <td><?php echo number_format($value_mutasi->akumulasi_penyusutan) ?></td>
                        <td><?php echo number_format($value_mutasi->nilai_buku) ?></td>
                        <td><?php echo $value_mutasi->keterangan; ?></td>
                        <td>
                            <a href="#" title="Hapus" onclick="hapus_data(<?php echo $value_mutasi->id_pengapusan; ?>)"><i class="md-icon material-icons" style="color:green;">delete</i></a>

                        </td>
                    </tr>


                <?php } ?>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>



<?php $this->load->view('user/layer/body_bawah'); ?>
<script src="<?php echo base_url('/assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<!-- datatables colVis-->
<script src="<?php echo base_url('/assets/bower_components/datatables-colvis/js/dataTables.colVis.js') ?>"></script>
<!-- datatables tableTools-->
<script src="<?php echo base_url('/assets/bower_components/datatables-tabletools/js/dataTables.tableTools.js') ?>"></script>
<!-- datatables custom integration -->
<script src="<?php echo base_url('/assets/assets/js/custom/datatables_uikit.min.js') ?>"></script>

<!--  datatables functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/plugins_datatables.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/pages/components_notifications.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/kendoui_custom.min.js') ?>"></script>

<!--  kendoui functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/kendoui.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js') ?>"></script>
<!-- htmleditor (codeMirror) -->
<script src="<?php echo base_url('/assets/assets/js/uikit_htmleditor_custom.min.js'); ?>"></script>
<!-- inputmask-->
<script src="<?php echo base_url('/assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js') ?>"></script>

<!--  forms advanced functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/forms_advanced.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/bower_components/parsleyjs/dist/parsley.min.js') ?>"></script>
<!-- jquery steps -->
<script src="<?php echo base_url('/assets/assets/js/custom/wizard_steps.min.js') ?>"></script>

<!--  forms wizard functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/forms_wizard.min.js') ?>"></script>
<script>
    function rubah(angka) {
        var reverse = angka.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join('.').split('').reverse().join('');
        return ribuan;
    }

    function cari_harga() {
        var id_barang = $('.kode_barangg option:selected').attr("value");
        $.ajax({
            type: 'AJAX',
            url: '<?php echo base_url('/user/Barang/view_edit_barang/') ?>' + id_barang,
            async: false,
            dataType: 'json',
            success: function(data) {
                console.log(data);
                $('#harga_barangkan').val((rubah(data.view_edit_barang.harga)));
            }

        });

    };


    function tambahkan_barang_hapus() {
        var id_barang = $('.kode_barangg option:selected').attr("value");
        var id_keputusan = $('#id_keputusan').val();
        var nilai = $('#nilai').val();
        var akumulasi = $('#akumulasi').val();
        var nilai_buku = $('#nilai_buku').val();

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/penghapusan/simpan_barang_penghapusan') ?>",
            data: {
                id_barang: id_barang,
                id_keputusan: id_keputusan,
                nilai: nilai,
                akumulasi: akumulasi,
                nilai_buku: nilai_buku
            },
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/penghapusan/add_data_penghapusan/") . $view_keputusan->id_keputusan; ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/penghapusan/add_data_penghapusan/") . $view_keputusan->id_keputusan; ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);
    };


    function simpan_mutasi() {
        alert('Anda yakin untuk menyimpan penghapusan ini...');
        var id_keputusan = $('#id_keputusan').val();

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/penghapusan/simpan_penghapusan') ?>",
            data: {
                id_keputusan: id_keputusan,
            },
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/penghapusan/add_data_penghapusan/") . $view_keputusan->id_keputusan; ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/penghapusan/add_data_penghapusan/") . $view_keputusan->id_keputusan; ?>');
                    }, 1000);
                }

            },

        });
    };

    function hapus_data(kode) {
        var txt;
        var r = confirm("Anda yakin menghapus data ini!");
        if (r == true) {
            txt = "You pressed OK!";
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url('/user/penghapusan/hapus_penghapusancoy/') ?>' + kode,
                success: function(data) {

                    if (data == 'sukses') {
                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Berhasil Dihapus...',
                                title: '',
                                type: 'success',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/penghapusan/add_data_penghapusan/") . $view_keputusan->id_keputusan; ?>');
                        }, 1000);
                        console.log(data);

                    } else {

                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Gagal Dihapus...',
                                title: '',
                                type: 'error',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/penghapusan/add_data_penghapusan/") .  $view_keputusan->id_keputusan; ?>');
                        }, 1000);
                    }
                    console.log(data);
                }

            })
            console.log(kode);
        } else {

        }
    }
</script>


<?php $this->load->view('user/layer/footer'); ?>