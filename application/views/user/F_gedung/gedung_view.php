<?php $this->load->view('user/layer/header') ?>
<!-- tempat css/javascript -->
<style>
    /* kosong */
</style>
<?php $this->load->view('user/layer/body_atas') ?>

<div id="page_content_inner">
    <h3 class="heading_b uk-margin-bottom">DASHBOARD > GEDUNG</h3>
    <div class="md-card">
        <div class="uk-width-medium-1-6" style="padding-left: 890px;">
            <a class="md-btn md-btn-primary" href="#" style="width: max-content;" title="Tambah Gedung" data-uk-modal="{target:'#tambah_gedung'}">Tambah Gedung</a>
        </div>
        <div class="md-card-content">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="gedung_table" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Kode Lokasi</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="#" class="#">
                            <?php $no = 1;
                            foreach ($gedung as $valuekan) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $valuekan->nama; ?></td>
                                    <td><?php echo $valuekan->alamat; ?></td>
                                    <td><?php echo $valuekan->kode_lokasi; ?></td>
                                    <td>
                                        <a href="#" onclick="view_edit(<?php echo $valuekan->id; ?>)" data-uk-modal="{target:'#edit_gedun'}"><i class="md-icon material-icons" style="color:green;">create</i></a>
                                        <a href="#" onclick="hapus_data(<?php echo $valuekan->id; ?>)"><i class="md-icon material-icons" style="color:red;">delete</i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div id="hasil"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="md-fab-wrapper">
    <a class="md-fab md-fab-success" href="#" data-uk-tooltip="{pos:'right'}" title="Tambah Gedung" data-uk-modal="{target:'#tambah_gedung'}"><i class="material-icons">playlist_add</i></a>
</div> -->


<div class="uk-modal" id="tambah_gedung">
    <form action="" method="POST" id="input_gedung">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Tambah Data Gedung</h3>
            </div>
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-2-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <label>NAMA GEDUNG</label>
                                    <input type="text" name="nama_gedung" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>ALAMAT GEDUNG</label>
                                    <input type="text" name="alamat_gedung" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>KODE LOKASI</label>
                                    <input type="text" name="lokasi_gedung" required class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close" onclick="input_gedungkan()">SIMPAN</button>
            </div>
        </div>
    </form>
</div>

<!-- edit -->

<div class="uk-modal" id="edit_gedun">
    <form action="" method="POST" id="input_edit_gedung">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Edit Data Gedung</h3>
            </div>
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-2-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <label>NAMA GEDUNG</label>
                                    <input type="text" name="edit_nama_gedung" id="edit_nama_gedung" required class="md-input label-fixed" />
                                    <input type="hidden" name="edit_id_gedung" id="edit_id_gedung">
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>ALAMAT GEDUNG</label>
                                    <input type="text" name="edit_alamat_gedung" id="edit_alamat_gedung" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>KODE LOKASI</label>
                                    <input type="text" name="edit_lokasi_gedung" id="edit_lokasi_gedung" required class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close" onclick="input_edit_gedungkan()">SIMPAN</button>
            </div>
        </div>
    </form>
</div>

<?php $this->load->view('user/layer/body_bawah'); ?>
<script src="<?php echo base_url('/assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<!-- datatables colVis-->
<script src="<?php echo base_url('/assets/bower_components/datatables-colvis/js/dataTables.colVis.js') ?>"></script>
<!-- datatables tableTools-->
<script src="<?php echo base_url('/assets/bower_components/datatables-tabletools/js/dataTables.tableTools.js') ?>"></script>
<!-- datatables custom integration -->
<script src="<?php echo base_url('/assets/assets/js/custom/datatables_uikit.min.js') ?>"></script>

<!--  datatables functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/plugins_datatables.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/pages/components_notifications.min.js') ?>"></script>
<!-- tempat javascript -->
<script type="text/javascript">
    $(document).ready(function() {
        // view_gedung();
        $("#gedung_table").dataTable();


    });

    function input_gedungkan() {
        var data = $('#input_gedung').serialize();
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/Gedung/simpan_gedung') ?>",
            data: data,
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Gedung"); ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Gedung"); ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);
    }

    function view_edit(kode) {

        $.ajax({
            type: 'GET',
            url: '<?php echo base_url('/user/gedung/view_edit/') ?>' + kode,
            dataType: "JSON",
            success: function(data) {
                document.getElementById('edit_id_gedung').value = data.view_edit_gedung.id;
                document.getElementById('edit_nama_gedung').value = data.view_edit_gedung.nama;
                document.getElementById('edit_alamat_gedung').value = data.view_edit_gedung.alamat;
                document.getElementById('edit_lokasi_gedung').value = data.view_edit_gedung.kode_lokasi;

                console.log(data);
            }

        })
        console.log(kode);
    }

    function input_edit_gedungkan() {

        var data = $('#input_edit_gedung').serialize();
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/Gedung/simpan_edit_gedung') ?>",
            data: data,
            success: function(data) {

                if (data == 'sukses') {
                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Diupdate...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Gedung"); ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Diupdate...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Gedung"); ?>');
                    }, 1000);
                }

            },

        });

    };

    function hapus_data(kode) {
        var txt;
        var r = confirm("Anda yakin menghapus data ini!");
        if (r == true) {
            txt = "You pressed OK!";
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url('/user/gedung/hapus_gedung/') ?>' + kode,
                success: function(data) {

                    if (data == 'sukses') {
                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Berhasil Dihapus...',
                                title: '',
                                type: 'success',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Gedung"); ?>');
                        }, 1000);
                        console.log(data);

                    } else {

                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Gagal Dihapus...',
                                title: '',
                                type: 'error',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Gedung"); ?>');
                        }, 1000);
                    }
                    console.log(data);
                }

            })
            console.log(kode);
        } else {

        }
    }
</script>
<!-- tempat javascript -->
<?php $this->load->view('user/layer/footer'); ?>