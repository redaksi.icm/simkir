<?php $this->load->view('user/layer/header') ?>
<!-- tempat css/javascript -->
<link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/kendo-ui/styles/kendo.common-material.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/kendo-ui/styles/kendo.material.min.css') ?>" />
<style>
    /* kosong */
</style>
<?php $this->load->view('user/layer/body_atas') ?>

<div id="page_content_inner">
    <h3 class="heading_b uk-margin-bottom">DASHBOARD > KIB > VIEW MUTASI BARANG</h3>
    <div class="md-card">
        <h3 class="uk-text-center">KARTU INVENTARIS BARANG (KIB)</h3>
        <h3 class="uk-text-center" style="margin-top: 0;">PERALATAN DAN MESIN</h3>
        <div class="md-card-content">
            <table id="" class="uk" cellspacing="0" style="width: 100%;max-width: 100%;font-size: 17px;">
                <tr>
                    <td style="width: 10%;">DAFTAR</td>
                    <td style="width: 1%;">:</td>
                    <td>
                        <?php echo $view_kib->daftar; ?>
                        <input type="hidden" name="id_kibkib" id="id_kibkib" value="<?php echo $view_kib->id_kib; ?>">
                    </td>
                </tr>
                <tr>
                    <td>NOMOR</td>
                    <td>:</td>
                    <td><?php echo $view_kib->nomor; ?></td>
                </tr>
                <tr>
                    <td>TANGGAL</td>
                    <td>:</td>
                    <td><?php echo $view_kib->tanggal; ?></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <table id="mutasi_table" class="uk-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Tahun Pembelian</th>
                        <th>Jumlah Register</th>
                        <th>Harga</th>
                        <th>Nilai</th>
                        <th>Akumulasi Penyusutan</th>
                        <th>Nilai BUku</th>
                        <!-- <th>Baik</th>
                        <th>Kurang Baik</th>
                        <th>Kondisi Rusak</th> -->
                        <th>Ket</th>
                    </tr>
                </thead>
                <?php
                $no = 1;
                foreach ($mutasi as $value_mutasi) {

                ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $value_mutasi->kode_barang; ?></td>
                        <td><?php echo $value_mutasi->nama; ?></td>
                        <td><?php echo $value_mutasi->thn_pembelian; ?></td>
                        <td><?php echo $value_mutasi->jumlah_register; ?></td>
                        <td><?php echo number_format($value_mutasi->harga, 2); ?></td>
                        <td><?php echo number_format($value_mutasi->nilai) ?></td>
                        <td><?php echo number_format($value_mutasi->akumulasi_penyusutan) ?></td>
                        <td><?php echo number_format($value_mutasi->nilai_buku) ?></td>
                        <!-- <td><?php echo $value_mutasi->jumlah_register; ?></td>
                        <td>-</td>
                        <td>-</td> -->
                        <td><?php echo $value_mutasi->keterangan; ?></td>
                    </tr>


                <?php } ?>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>


<?php $this->load->view('user/layer/body_bawah'); ?>
<script src="<?php echo base_url('/assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<!-- datatables colVis-->
<script src="<?php echo base_url('/assets/bower_components/datatables-colvis/js/dataTables.colVis.js') ?>"></script>
<!-- datatables tableTools-->
<script src="<?php echo base_url('/assets/bower_components/datatables-tabletools/js/dataTables.tableTools.js') ?>"></script>
<!-- datatables custom integration -->
<script src="<?php echo base_url('/assets/assets/js/custom/datatables_uikit.min.js') ?>"></script>

<!--  datatables functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/plugins_datatables.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/pages/components_notifications.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/kendoui_custom.min.js') ?>"></script>

<!--  kendoui functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/kendoui.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js') ?>"></script>
<!-- htmleditor (codeMirror) -->
<script src="<?php echo base_url('/assets/assets/js/uikit_htmleditor_custom.min.js'); ?>"></script>
<!-- inputmask-->
<script src="<?php echo base_url('/assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js') ?>"></script>

<!--  forms advanced functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/forms_advanced.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/bower_components/parsleyjs/dist/parsley.min.js') ?>"></script>
<!-- jquery steps -->
<script src="<?php echo base_url('/assets/assets/js/custom/wizard_steps.min.js') ?>"></script>

<!--  forms wizard functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/forms_wizard.min.js') ?>"></script>


<!-- tempat javascript -->
<script type="text/javascript">
    function rubah(angka) {
        var reverse = angka.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join('.').split('').reverse().join('');
        return ribuan;
    }

    function cari_harga() {
        var id_barang = $('.kode_barangg option:selected').attr("value");
        $.ajax({
            type: 'AJAX',
            url: '<?php echo base_url('/user/Barang/view_edit_barang/') ?>' + id_barang,
            async: false,
            dataType: 'json',
            success: function(data) {
                console.log(data);
                $('#harga_barangkan').val((rubah(data.view_edit_barang.harga)));
            }

        });

    }

    function tambahkan_barang_mutasi() {
        var id_barang = $('.kode_barangg option:selected').attr("value");
        var id_kibkib = $('#id_kibkib').val();
        var nilai = $('#nilai').val();
        var akumulasi = $('#akumulasi').val();
        var nilai_buku = $('#nilai_buku').val();

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/Mutasi/simpan_barang_mutasi') ?>",
            data: {
                id_barang: id_barang,
                id_kibkib: id_kibkib,
                nilai: nilai,
                akumulasi: akumulasi,
                nilai_buku: nilai_buku
            },
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Mutasi/add_data_mutasi/") . $view_kib->id_kib; ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Mutasi/add_data_mutasi/") . $view_kib->id_kib; ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);

        console.log(id_barang, id_kibkib, nilai, akumulasi, nilai_buku);
    };

    function simpan_mutasi() {
        alert('Anda yakin untuk menyimpan mutasi ini...');
        var id_kibkib = $('#id_kibkib').val();

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/Mutasi/simpan_mutasi') ?>",
            data: {
                id_kibkib: id_kibkib,
            },
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Mutasi/") ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Mutasi/") ?>');
                    }, 1000);
                }

            },

        });
    }
</script>

<?php $this->load->view('user/layer/footer'); ?>