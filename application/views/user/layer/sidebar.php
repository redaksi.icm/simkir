<div class="menu_section">
    <ul>
        <li title="Dashboard">
            <a href="<?php echo base_url('user/Home'); ?>">
                <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                <span class="menu_title">Dashboard</span>
            </a>
        </li>
        <hr class="uk-grid-divider" style="margin-top: 0px;margin-bottom: 0px;">

        <?php if ($this->session->userdata('jabatan') == 'admin') { ?>
            <li title="Data Master">
                <a href="#">
                    <span class="menu_title">DATA MASTER</span>
                </a>
            </li>
            <li title="Gedung">
                <a href="<?php echo base_url('user/Gedung'); ?>">
                    <span class="menu_icon">
                        <span class="material-icons">
                            account_balance
                        </span></span>
                    <span class="menu_title">Gedung</span>
                </a>
            </li>
            <li title="Gedung">
                <a href="<?php echo base_url('user/Ruang'); ?>">
                    <span class="menu_icon">
                        <span class="material-icons">
                            home
                        </span></span>
                    <span class="menu_title">Ruangan</span>
                </a>
            </li>
            <li title="Barang">
                <a href="<?php echo base_url('user/Barang'); ?>">
                    <span class="menu_icon">
                        <span class="material-icons">
                            add_shopping_cart
                        </span></span>
                    <span class="menu_title">Barang</span>
                </a>
            </li>
            <li title="User">
                <a href="<?php echo base_url('user/User'); ?>">
                    <span class="menu_icon">
                        <span class="material-icons">
                            Users
                        </span></span>
                    <span class="menu_title">User</span>
                </a>
            </li>
            <hr class="uk-grid-divider" style="margin-top: 0px;margin-bottom: 0px;">
            <li title="Data Master">
                <a href="#">
                    <span class="menu_title">PROSES DATA</span>
                </a>
            </li>
            <li title="Kartu Inventaris Ruangan">
                <a href="<?php echo base_url('user/Inventaris'); ?>" style="font-size: 12px;">
                    <span class="menu_icon">
                        <span class="material-icons">
                            spellcheck
                        </span></span>
                    <span class="menu_title">Kartu Inventaris Ruangan</span>
                </a>
            </li>
            <li title="Laporan KIR">
                <a href="<?php echo base_url('user/Laporan_kir'); ?>" style="font-size: 12px;">
                    <span class="menu_icon">
                        <span class="material-icons">
                            import_contacts
                        </span></span>
                    <span class="menu_title">Laporan KIR</span>
                </a>
            </li>
            <li title="Mutasi Barang">
                <a href="<?php echo base_url('user/Mutasi'); ?>" style="font-size: 12px;">
                    <span class="menu_icon">
                        <span class="material-icons">
                            gavel
                        </span></span>
                    <span class="menu_title">Mutasi Barang</span>
                </a>
            </li>
            <li title="Mutasi Barang">
                <a href="<?php echo base_url('user/Penghapusan'); ?>" style="font-size: 12px;">
                    <span class="menu_icon">
                        <span class="material-icons">
                            gavel
                        </span></span>
                    <span class="menu_title">Penghapusan Barang</span>
                </a>
            </li>
            <li title="Perbaikan Barang">
                <a href="<?php echo base_url('user/Perbaikan'); ?>" style="font-size: 12px;">
                    <span class="menu_icon">
                        <span class="material-icons">
                            gavel
                        </span></span>
                    <span class="menu_title">Perbaikan Barang</span>
                </a>
            </li>

        <?php } else { ?>
            <li title="Data Master">
                <a href="#">
                    <span class="menu_title">DATA MASTER</span>
                </a>
            </li>
            <li title="Barang">
                <a href="<?php echo base_url('user/Barang'); ?>">
                    <span class="menu_icon">
                        <span class="material-icons">
                            add_shopping_cart
                        </span></span>
                    <span class="menu_title">Barang</span>
                </a>
            </li>
            <hr class="uk-grid-divider" style="margin-top: 0px;margin-bottom: 0px;">
            <li title="Data Master">
                <a href="#">
                    <span class="menu_title">PROSES DATA</span>
                </a>
            </li>
            <li title="Kartu Inventaris Ruangan">
                <a href="<?php echo base_url('user/Inventaris'); ?>" style="font-size: 12px;">
                    <span class="menu_icon">
                        <span class="material-icons">
                            spellcheck
                        </span></span>
                    <span class="menu_title">Kartu Inventaris Ruangan</span>
                </a>
            </li>
            <li title="Laporan KIR">
                <a href="<?php echo base_url('user/Laporan_kir'); ?>" style="font-size: 12px;">
                    <span class="menu_icon">
                        <span class="material-icons">
                            import_contacts
                        </span></span>
                    <span class="menu_title">Laporan KIR</span>
                </a>
            </li>
            <li title="Mutasi Barang">
                <a href="<?php echo base_url('user/Mutasi'); ?>" style="font-size: 12px;">
                    <span class="menu_icon">
                        <span class="material-icons">
                            gavel
                        </span></span>
                    <span class="menu_title">Mutasi Barang</span>
                </a>
            </li>
            <li title="Mutasi Barang">
                <a href="<?php echo base_url('user/Penghapusan'); ?>" style="font-size: 12px;">
                    <span class="menu_icon">
                        <span class="material-icons">
                            gavel
                        </span></span>
                    <span class="menu_title">Penghapusan Barang</span>
                </a>
            </li>
            <li title="Perbaikan Barang">
                <a href="<?php echo base_url('user/Perbaikan'); ?>" style="font-size: 12px;">
                    <span class="menu_icon">
                        <span class="material-icons">
                            gavel
                        </span></span>
                    <span class="menu_title">Perbaikan Barang</span>
                </a>
            </li>

        <?php } ?>
    </ul>
</div>