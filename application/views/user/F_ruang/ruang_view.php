<?php $this->load->view('user/layer/header') ?>
<link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/kendo-ui/styles/kendo.common-material.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/kendo-ui/styles/kendo.material.min.css') ?>" />
<!-- tempat css/javascript -->
<style>
    /* kosong */
</style>
<?php $this->load->view('user/layer/body_atas') ?>

<div id="page_content_inner">
    <h3 class="heading_b uk-margin-bottom">DASHBOARD > RUANGAN</h3>
    <div class="md-card">
        <div class="uk-width-medium-1-6" style="padding-left: 890px;">
            <a class="md-btn md-btn-primary" href="#" style="width: max-content;" title="Tambah Gedung" data-uk-modal="{target:'#tambah_ruangan'}">Tambah Ruangan</a>
        </div>
        <div class="md-card-content">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="ruangan_table" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Ruangan</th>
                                <th>Nama Gedung</th>
                                <th>Kode Lokasi</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="show_ruangan" class="show_ruangan">
                            <?php $no = 1;
                            foreach ($ruangan as $value) {

                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $value->nama_ruangan; ?></td>
                                    <td><?php echo $value->nama; ?></td>
                                    <td><?php echo $value->kode_lokasi; ?></td>
                                    <td>
                                        <a href="#" onclick="view_edit(<?php echo $value->id_ruangan; ?>)" data-uk-modal="{target:'#tambah_edit_ruangan'}"><i class="md-icon material-icons" style="color:green;">create</i></a>
                                        <a href="#" onclick="hapus_data(<?php echo $value->id_ruangan; ?>)"><i class="md-icon material-icons" style="color:red;">delete</i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- batas -->
<!-- <div class="md-fab-wrapper">
    <a class="md-fab md-fab-success" href="#" data-uk-tooltip="{pos:'right'}" title="Tambah Ruangan" data-uk-modal="{target:'#tambah_ruangan'}"><i class="material-icons">playlist_add</i></a>
</div> -->

<!-- input -->
<div class="uk-modal" id="tambah_ruangan">
    <form action="" method="POST" id="input_ruangan">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Tambah Data Ruangan</h3>
            </div>
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-2-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <label>NAMA GEDUNG</label><br>
                                    <select id="pilih_gedung" name="pilih_gedung" class="md-input pilih_gedung">
                                        <option value="0">Pilih Gedung</option>
                                    </select>
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>NAMA RUANGAN</label>
                                    <input type="text" name="nama_ruangan" required class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close" onclick="input_ruangankan()">SIMPAN</button>
            </div>
        </div>
    </form>
</div>

<div class="uk-modal" id="tambah_edit_ruangan">
    <form action="" method="POST" id="input_edit_ruangan">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Edit Data Ruangan</h3>
            </div>
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-2-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <label>NAMA GEDUNG</label><br>
                                    <select id="pilih_gedung_edit" name="pilih_gedung_edit" class="md-input pilih_gedung">
                                        <option value="0">Pilih Gedung</option>
                                    </select>
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>NAMA RUANGAN</label>
                                    <input type="text" name="nama_ruangan_edit" id="nama_ruangan_edit" required class="md-input label-fixed" />
                                    <input type="hidden" id="id_ruangan_edit" name="id_ruangan_edit">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close" onclick="input_edit_ruangankan()">SIMPAN</button>
            </div>
        </div>
    </form>
</div>

<?php $this->load->view('user/layer/body_bawah'); ?>
<script src="<?php echo base_url('/assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<!-- datatables colVis-->
<script src="<?php echo base_url('/assets/bower_components/datatables-colvis/js/dataTables.colVis.js') ?>"></script>
<!-- datatables tableTools-->
<script src="<?php echo base_url('/assets/bower_components/datatables-tabletools/js/dataTables.tableTools.js') ?>"></script>
<!-- datatables custom integration -->
<script src="<?php echo base_url('/assets/assets/js/custom/datatables_uikit.min.js') ?>"></script>

<!--  datatables functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/plugins_datatables.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/pages/components_notifications.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/kendoui_custom.min.js') ?>"></script>

<!--  kendoui functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/kendoui.min.js') ?>"></script>


<!-- tempat javascript -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#ruangan_table").dataTable();
        // view_ruangan();

        // function view_ruangan() {
        //     $.ajax({
        //         type: 'AJAX',
        //         url: '<?php echo base_url() ?>user/Ruang/view_data_ruangan',
        //         async: false,
        //         dataType: 'json',
        //         success: function(data) {
        //             var html = '';
        //             var i;
        //             var x = 1;
        //             for (i = 0; i < data.ruangan.length; i++) {
        //                 html += '<tr>' +
        //                     '<td>' + [x++] + '</td>' +
        //                     '<td>' + data.ruangan[i].nama_ruangan + '</td>' +
        //                     '<td>' + data.ruangan[i].nama + '</td>' +
        //                     '<td>' + data.ruangan[i].kode_lokasi + '</td>' +
        //                     '<td class="">' +
        //                     '<a href="#" onclick="view_edit(' + data.ruangan[i].id_ruangan + ')" data-uk-modal="{target:' + "'#tambah_edit_ruangan'" + '}"><i class="md-icon material-icons" style="color:green;">create</i></a>' +
        //                     '<a href="#" onclick="hapus_data(' + data.ruangan[i].id_ruangan + ')" ><i class="md-icon material-icons" style="color:red;">delete</i></a>' +
        //                     '</td>' +
        //                     '</tr>';

        //             }
        //             $('#show_ruangan').html(html);
        //         }

        //     });
        // };

    });

    $(window).load(function() {
        $.ajax({
            dataType: "json",
            url: '<?php echo base_url('user/Ruang/view_gedung_ruangan'); ?>',
            success: function(data) {
                console.log(data);
                var ruang = jQuery.parseJSON(JSON.stringify(data));
                $.each(ruang, function(v, k) {
                    $('.pilih_gedung')
                        .append($("<option></option>")
                            .attr("value", k.id)
                            .text(k.nama));
                });
            }
        });
    });


    function input_ruangankan() {
        var data = $('#input_ruangan').serialize();
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/Ruang/simpan_ruangan') ?>",
            data: data,
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Ruang"); ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Ruang"); ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);
    };

    function view_edit(kode) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url('/user/ruang/view_edit_ruang/') ?>' + kode,
            dataType: "JSON",
            success: function(data) {
                document.getElementById('id_ruangan_edit').value = data.view_edit_ruang.id_ruangan;
                document.getElementById('nama_ruangan_edit').value = data.view_edit_ruang.nama_ruangan;
                $('#pilih_gedung_edit').val(data.view_edit_ruang.id).change();
                console.log(data);
            }
        })
        console.log(kode);
    };

    function input_edit_ruangankan() {

        var data = $('#input_edit_ruangan').serialize();
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/Ruang/simpan_edit_ruangan') ?>",
            data: data,
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Ruang"); ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Ruang"); ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);

    };

    function hapus_data(kode) {
        var txt;
        var r = confirm("Anda yakin menghapus data ini!");
        if (r == true) {
            txt = "You pressed OK!";
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url('/user/Ruang/hapus_ruangan/') ?>' + kode,
                success: function(data) {

                    if (data == 'sukses') {
                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Berhasil Dihapus...',
                                title: '',
                                type: 'success',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Ruang"); ?>');
                        }, 1000);
                        console.log(data);

                    } else {

                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Gagal Dihapus...',
                                title: '',
                                type: 'error',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Ruang"); ?>');
                        }, 1000);
                    }
                    console.log(data);
                }

            })
        } else {

        }
    }
</script>
<?php $this->load->view('user/layer/footer'); ?>