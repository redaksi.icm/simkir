<?php $this->load->view('user/layer/header') ?>
<!-- tempat css/javascript -->
<style>
    /* kosong */
</style>
<?php $this->load->view('user/layer/body_atas') ?>
<div id="page_content_inner">
    <h3 class="heading_b uk-margin-bottom">DASHBOARD > BARANG</h3>
    <div class="md-card">
        <div class="uk-width-medium-1-6" style="padding-left: 890px;">
            <a class="md-btn md-btn-primary" href="#" style="width: max-content;" title="Tambah Gedung" data-uk-modal="{target:'#tambah_barang'}">Tambah Barang</a>
        </div>
        <div class="md-card-content">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="barang_table" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Merek</th>
                                <th>No Seri</th>
                                <th>Ukuran</th>
                                <th>Bahan</th>
                                <th>Tahun Pembelian</th>
                                <th>Harga</th>
                                <!-- <th>Jumlah</th> -->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="show_barang" class="show_barang">
                            <?php $no = 1;
                            foreach ($barang as $value) { ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $value->kode_barang; ?></td>
                                    <td><?php echo $value->nama; ?></td>
                                    <td><?php echo $value->merek; ?></td>
                                    <td><?php echo $value->no_seri; ?></td>
                                    <td><?php echo $value->ukuran; ?></td>
                                    <td><?php echo $value->bahan; ?></td>
                                    <td><?php echo $value->thn_pembelian; ?></td>
                                    <td><?php echo number_format($value->harga); ?></td>
                                    <td>
                                        <a href="#" onclick="view_edit_barang(<?php echo $value->id; ?>)" data-uk-modal="{target:'#edit_barang'}"><i class="md-icon material-icons" style="color:green;">create</i></a>
                                        <a href="#" onclick="hapus_data(<?php echo $value->id; ?>)"><i class="md-icon material-icons" style="color:red;">delete</i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- batas -->
<!-- <div class="md-fab-wrapper">
    <a class="md-fab md-fab-success" href="#" data-uk-tooltip="{pos:'right'}" title="Tambah Barang" data-uk-modal="{target:'#tambah_barang'}"><i class="material-icons">playlist_add</i></a>
</div> -->
<!-- tambah barang -->
<div class="uk-modal" id="tambah_barang">
    <form action="" method="POST" id="input_barang">
        <div class="uk-modal-dialog" style="width: 100%; max-width: 1000px;">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Tambah Data Barang</h3>
            </div>
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <label>KODE BARANG</label>
                                    <input type="text" name="kode_barang" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>NAMA BARANG</label>
                                    <input type="text" name="nama_barang" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>MEREK BARANG</label>
                                    <input type="text" name="merek_barang" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>NO. SERI</label>
                                    <input type="text" name="no_seri" required class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <label>UKURAN</label>
                                    <input type="text" name="ukuran" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>BAHAN</label>
                                    <input type="text" name="bahan" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>TAHUN PEMBELIAN</label>
                                    <input type="text" name="tahun_pembelian" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>HARGA</label>
                                    <input type="text" name="harga_barang" required class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close" onclick="input_barangkan()">SIMPAN</button>
            </div>
        </div>
    </form>
</div>
<!-- edit -->
<div class="uk-modal" id="edit_barang">
    <form action="" method="POST" id="input_edit_barang">
        <div class="uk-modal-dialog" style="width: 100%; max-width: 1000px;">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Edit Data Barang</h3>
            </div>
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <label>KODE BARANG</label>
                                    <input type="text" name="kode_barang_edit" id="kode_barang_edit" required class="md-input label-fixed" />
                                    <input type="hidden" name="id_barang_edit" id="id_barang_edit" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>NAMA BARANG</label>
                                    <input type="text" name="nama_barang_edit" id="nama_barang_edit" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>MEREK BARANG</label>
                                    <input type="text" name="merek_barang_edit" id="merek_barang_edit" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>NO. SERI</label>
                                    <input type="text" name="no_seri_edit" id="no_seri_edit" required class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <label>UKURAN</label>
                                    <input type="text" name="ukuran_edit" id="ukuran_edit" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>BAHAN</label>
                                    <input type="text" name="bahan_edit" id="bahan_edit" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>TAHUN PEMBELIAN</label>
                                    <input type="text" name="tahun_pembelian_edit" id="tahun_pembelian_edit" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>HARGA</label>
                                    <input type="text" name="harga_barang_edit" id="harga_barang_edit" required class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close" onclick="input_edit_barangkan()">SIMPAN</button>
            </div>
        </div>
    </form>
</div>

<?php $this->load->view('user/layer/body_bawah'); ?>
<script src="<?php echo base_url('/assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<!-- datatables colVis-->
<script src="<?php echo base_url('/assets/bower_components/datatables-colvis/js/dataTables.colVis.js') ?>"></script>
<!-- datatables tableTools-->
<script src="<?php echo base_url('/assets/bower_components/datatables-tabletools/js/dataTables.tableTools.js') ?>"></script>
<!-- datatables custom integration -->
<script src="<?php echo base_url('/assets/assets/js/custom/datatables_uikit.min.js') ?>"></script>

<!--  datatables functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/plugins_datatables.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/pages/components_notifications.min.js') ?>"></script>
<!-- tempat javascript -->
<script type="text/javascript">
    $(document).ready(function() {
        function rubah(angka) {
            var reverse = angka.toString().split('').reverse().join(''),
                ribuan = reverse.match(/\d{1,3}/g);
            ribuan = ribuan.join('.').split('').reverse().join('');
            return ribuan;
        }
        $('#barang_table').dataTable();
        // view_barang();

        // function view_barang() {
        //     $.ajax({
        //         type: 'AJAX',
        //         url: '<?php echo base_url() ?>user/Barang/view_data_barang',
        //         async: false,
        //         dataType: 'json',
        //         success: function(data) {
        //             var html = '';
        //             var i;
        //             var x = 1;
        //             for (i = 0; i < data.barang.length; i++) {
        //                 html += '<tr>' +
        //                     '<td>' + [x++] + '</td>' +
        //                     '<td>' + data.barang[i].kode_barang + '</td>' +
        //                     '<td>' + data.barang[i].nama + '</td>' +
        //                     '<td>' + data.barang[i].merek + '</td>' +
        //                     '<td>' + data.barang[i].no_seri + '</td>' +
        //                     '<td>' + data.barang[i].ukuran + '</td>' +
        //                     '<td>' + data.barang[i].bahan + '</td>' +
        //                     '<td>' + data.barang[i].thn_pembelian + '</td>' +
        //                     '<td>' + 'Rp. ' + (rubah(data.barang[i].harga)) + '</td>' +
        //                     '<td class="">' +
        //                     '<a href="#" onclick="view_edit_barang(' + data.barang[i].id + ')" data-uk-modal="{target:' + "'#edit_barang'" + '}"><i class="md-icon material-icons" style="color:green;">create</i></a>' +
        //                     '<a href="#" onclick="hapus_data(' + data.barang[i].id + ')" ><i class="md-icon material-icons" style="color:red;">delete</i></a>' +
        //                     '</td>' +
        //                     '</tr>';

        //             }
        //             $('#show_barang').html(html);
        //         }

        //     });
        // }

    });

    function input_barangkan() {
        var data = $('#input_barang').serialize();
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/Barang/simpan_barang') ?>",
            data: data,
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Barang"); ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Barang"); ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);
    };


    function view_edit_barang(kode) {

        $.ajax({
            type: 'GET',
            url: '<?php echo base_url('/user/Barang/view_edit_barang/') ?>' + kode,
            dataType: "JSON",
            success: function(data) {
                document.getElementById('id_barang_edit').value = data.view_edit_barang.id;
                document.getElementById('kode_barang_edit').value = data.view_edit_barang.kode_barang;
                document.getElementById('nama_barang_edit').value = data.view_edit_barang.nama;
                document.getElementById('merek_barang_edit').value = data.view_edit_barang.merek;
                document.getElementById('no_seri_edit').value = data.view_edit_barang.no_seri;
                document.getElementById('ukuran_edit').value = data.view_edit_barang.ukuran;
                document.getElementById('bahan_edit').value = data.view_edit_barang.bahan;
                document.getElementById('tahun_pembelian_edit').value = data.view_edit_barang.thn_pembelian;
                document.getElementById('harga_barang_edit').value = data.view_edit_barang.harga;

                console.log(data);
            }

        })
        console.log(kode);
    }

    function input_edit_barangkan() {

        var data = $('#input_edit_barang').serialize();
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/Barang/simpan_edit_barang') ?>",
            data: data,
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Barang"); ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Barang"); ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);

    };

    function hapus_data(kode) {
        var txt;
        var r = confirm("Anda yakin menghapus data ini!");
        if (r == true) {
            txt = "You pressed OK!";
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url('/user/Barang/hapus_barang/') ?>' + kode,
                success: function(data) {

                    if (data == 'sukses') {
                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Berhasil Dihapus...',
                                title: '',
                                type: 'success',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Barang"); ?>');
                        }, 1000);
                        console.log(data);

                    } else {

                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Gagal Dihapus...',
                                title: '',
                                type: 'error',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Barang"); ?>');
                        }, 1000);
                    }
                    console.log(data);
                }

            })
            console.log(kode);
        } else {

        }
    }
</script>

<?php $this->load->view('user/layer/footer'); ?>