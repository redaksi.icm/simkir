<?php $this->load->view('user/layer/header') ?>
<!-- tempat css/javascript -->
<style>
    /* kosong */
</style>
<?php $this->load->view('user/layer/body_atas') ?>
<div id="page_content_inner">
    <h3 class="heading_b uk-margin-bottom">DASHBOARD > KIR > TAMBAH BARANG</h3>
    <div class="uk-grid uk-grid-width-medium-6-2" data-uk-grid-margin="">
        <div class="uk-row-first">
            <div class="md-card md-card-primary">
                <div class="uk-width-medium-1-6" style="padding-left: 890px;">
                    <a class="md-btn md-btn-primary" href="#" style="width: max-content;" title="Tambah Gedung" data-uk-modal="{target:'#tambah_barang_kir'}">Tambah Barang</a>
                </div>
                <div class="md-card-content">

                    <div class="md-card-toolbar" style="background-color: #00923f;">
                        <h3 class="md-card-toolbar-heading-text" style="font-size: 24px;">
                            <label style="color: white;" for="">KETERANGAN KIR : </label>
                        </h3>
                    </div>


                    <table id="" class="uk-table" cellspacing="0" style="width: 100%;max-width: 100%;">
                        <tr style="background-color: #ddd;">
                            <td>NAMA RUANGAN</td>
                            <td>:</td>
                            <td><?php echo $view_add_barang_ruang->nama_ruangan; ?></td>
                            <td>|</td>
                            <td>KODE LOKASI</td>
                            <td>:</td>
                            <td><?php echo $view_add_barang_ruang->kode_lokasi; ?></td>
                        </tr>
                        <tr>
                            <td>NAMA GEDUNG</td>
                            <td>:</td>
                            <td><?php echo $view_add_barang_ruang->nama; ?></td>
                            <td>|</td>
                            <td>ALAMAT</td>
                            <td>:</td>
                            <td><?php echo $view_add_barang_ruang->alamat; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="md-card">
        <div class="md-card-content">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="barang_table_kir" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID</th>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Tahun Pembelian</th>
                                <th>Harga</th>
                                <th>Jumlah Register</th>
                                <th>Ket</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="show_barang_kir" class="show_barang_kir">
                            <?php
                            $no = 1;
                            foreach ($barang_kir_view as $data_kir) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data_kir->id_kir; ?></td>
                                    <td><?php echo $data_kir->kode_barang; ?></td>
                                    <td><?php echo $data_kir->nama; ?></td>
                                    <td><?php echo $data_kir->thn_pembelian; ?></td>
                                    <td>Rp. <?php echo number_format($data_kir->harga, 0, ',', '.'); ?></td>
                                    <td><?php echo $data_kir->jumlah_register; ?></td>
                                    <td><?php echo $data_kir->keterangan; ?></td>
                                    <td>
                                        <a href="#" id="recon_data_klik" title="Recon" data-uk-modal="{target:'#tambah_recon'}"><i class="md-icon material-icons" style="color:red;">check_circle</i></a>
                                        <a href="#" id="view_recon_kirkan" title="View Recon" data-uk-modal="{target:'#view_recon'}"><i class="md-icon material-icons" style="color:red;">toc</i></a>
                                        <a href="#" title="Edit Recon" id="edit_barang_kir_edit" data-uk-modal="{target:'#edit_barang_kir'}"><i class="md-icon material-icons" style="color:green;">create</i></a>
                                        <a href="#" title="Hapus Recon" onclick="hapus_data(<?php echo $data_kir->id_kir; ?>);"><i class="md-icon material-icons" style="color:green;">delete</i></a>
                                    </td>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="md-fab-wrapper">
    <a class="md-fab md-fab-success" href="#" data-uk-tooltip="{pos:'right'}" title="Tambah Barang KIR" data-uk-modal="{target:'#tambah_barang_kir'}"><i class="material-icons">playlist_add</i></a>
</div> -->

<!-- tambah barang -->
<div class="uk-modal" id="tambah_barang_kir">
    <form action="" method="POST" id="input_barang_kir">
        <div class="uk-modal-dialog" style="width: 100%; max-width: 1000px;">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Tambah Data Barang KIR</h3>
            </div>
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <label>NAMA BARANG</label>
                                    <select id="pilih_kode_barang" name="pilih_kode_barang" class="md-input pilih_kode_barang" onclick="view_select_barang()">
                                        <option value="0">Pilih Barang...</option>
                                    </select>
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>KODE BARANG</label>
                                    <br>
                                    <input type="text" name="kode_barang" id="kode_barang" readonly style="background: #4a444014;" class="md-input label-fixed" />
                                    <input type="hidden" name="kode_ruangan" id="kode_ruangan" readonly value="<?php echo  $view_add_barang_ruang->id_ruangan; ?>" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>MEREK BARANG</label>
                                    <br>
                                    <input type="text" name="merek_barang" id="merek_barang" readonly style="background: #4a444014;" class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>NO. SERI</label>
                                    <br>
                                    <input type="text" name="no_seri" id="no_seri" readonly style="background: #4a444014;" class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>UKURAN</label>
                                    <br>
                                    <input type="text" name="ukuran" id="ukuran" readonly style="background: #4a444014;" class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>BAHAN</label>
                                    <br>
                                    <input type="text" name="bahan" id="bahan" readonly style="background: #4a444014;" class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>TAHUN PEMBELIAN</label>
                                    <br>
                                    <input type="text" name="tahun_pembelian" id="tahun_pembelian" readonly style="background: #4a444014;" class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>HARGA</label>
                                    <br>
                                    <input type="text" name="harga_barang" id="harga_barang" readonly style="background: #4a444014;" class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>JUMLAH REGISTER</label>
                                    <br>
                                    <input type="text" name="jumlah_regis" id="jumlah_regis" class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>KETERANGAN</label>
                                    <br>
                                    <input type="text" name="keterangan" id="keterangan" class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close" onclick="input_barangkan()">SIMPAN</button>
            </div>
        </div>
    </form>
</div>

<!-- edit -->
<div class="uk-modal" id="edit_barang_kir">
    <form action="" method="POST" id="input_edit_barang_kir">
        <div class="uk-modal-dialog" style="width: 100%; max-width: 1000px;">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Edit Data Barang KIR</h3>
            </div>
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>KODE BARANG</label>
                                    <br>
                                    <input type="text" name="kode_barang_edit" id="kode_barang_edit" readonly style="background: #4a444014;" class="md-input label-fixed" />
                                    <input type="hidden" name="kode_kir_edit" id="kode_kir_edit" readonly value="" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>NAMA BARANG</label>
                                    <br>
                                    <input type="text" name="nama_barang_edit" id="nama_barang_edit" readonly style="background: #4a444014;" class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>JUMLAH REGISTER</label>
                                    <br>
                                    <input type="text" name="jumlah_regis_edit" id="jumlah_regis_edit" class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>KETERANGAN</label>
                                    <br>
                                    <input type="text" name="keterangan_edit" id="keterangan_edit" class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close" onclick="input_barangkan_editkan()">SIMPAN</button>
            </div>
        </div>
    </form>
</div>
<!-- batas recon input -->
<div class="uk-modal" id="tambah_recon">
    <form action="" method="POST" id="input_recon">
        <div class="uk-modal-dialog">
            <button type="button" class="uk-modal-close uk-close uk-close-alt right" style="background-color: red;"></button>
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Recon Barang</h3>
            </div>
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>KODE BARANG</label>
                                    <input type="text" name="kode_barang_recon" id="kode_barang_recon" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>JUMLAH REGISTER</label>
                                    <input type="text" name="jumlah_regis_recon" id="jumlah_regis_recon" required class="md-input label-fixed" />
                                    <input type="hidden" name="id_kir_recon" id="id_kir_recon" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <label>TAHUN RECON</label><br>
                                    <select id="tahunan_recon" class="tahunan_recon" name="tahunan_recon" style="width: inherit;height: 30px;">
                                        <option value="0">Pilih Tahun...</option>
                                        <?php
                                        date_default_timezone_set('Asia/Jakarta');
                                        $tahunow = date('Y');
                                        for ($tahun = 2015; $tahun <= $tahunow + 2; $tahun++) {
                                            echo "<option  value='$tahun'>$tahun </option>";
                                        }

                                        ?>
                                    </select>
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br><br>
                                    <label>KONDISI BAIK</label>
                                    <input type="text" name="kondisik_baik_recon" id="kondisik_baik_recon" required class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>KONDISI KURANG BAIK</label>
                                    <input type="text" name="kondisi_kurang_baik_recon" id="kondisi_kurang_baik_recon" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>KONDISI RUSAK</label>
                                    <input type="text" name="kondisi_rusak_recon" id="kondisi_rusak_recon" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <br>
                                    <label>KETERANGAN</label>
                                    <input type="text" name="keterangan_recon" id="keterangan_recon" required class="md-input label-fixed" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close" onclick="input_recon_kir()">SIMPAN</button>
            </div>
        </div>
    </form>
</div>

<!-- view recon -->

<div class="uk-modal" id="view_recon">
    <form action="" method="POST" id="view_recon_form">
        <div class="uk-modal-dialog" style="width: fit-content;width: 60%;max-width: 100%;">
            <button type="button" class="uk-modal-close uk-close uk-close-alt right" style="background-color: red;"></button>
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">View Recon <br> Kode Barang : <a id="kode_barang_reconkankan"></a></h3>
            </div>
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <table id="viewrecon" class="uk-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>ID</th>
                                            <th>Kondisi Baik</th>
                                            <th>Kondisi Kurang Baik</th>
                                            <th>Kondisi Rusak</th>
                                            <th>Keterangan</th>
                                            <th>Tahun Recon</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="show_recon_kir" class="show_recon_kir">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<?php $this->load->view('user/layer/body_bawah'); ?>
<script src="<?php echo base_url('/assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<!-- datatables colVis-->
<script src="<?php echo base_url('/assets/bower_components/datatables-colvis/js/dataTables.colVis.js') ?>"></script>
<!-- datatables tableTools-->
<script src="<?php echo base_url('/assets/bower_components/datatables-tabletools/js/dataTables.tableTools.js') ?>"></script>
<!-- datatables custom integration -->
<script src="<?php echo base_url('/assets/assets/js/custom/datatables_uikit.min.js') ?>"></script>

<!--  datatables functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/plugins_datatables.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/pages/components_notifications.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/kendoui_custom.min.js') ?>"></script>

<!--  kendoui functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/kendoui.min.js') ?>"></script>


<!-- tempat javascript -->
<script type="text/javascript">
    $(window).load(function() {
        $.ajax({
            dataType: "json",
            url: '<?php echo base_url('user/Inventaris/view_add_barang'); ?>',
            success: function(data) {
                console.log(data);
                var ruang = jQuery.parseJSON(JSON.stringify(data));
                $.each(ruang, function(v, k) {
                    $('.pilih_kode_barang')
                        .append($("<option></option>")
                            .attr("value", k.id)
                            .text(k.nama));
                });
            }
        });
    });

    function view_select_barang() {
        var kode = $('#pilih_kode_barang').val();
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url('/user/Barang/view_edit_barang/') ?>' + kode,
            dataType: "JSON",
            success: function(data) {
                document.getElementById('kode_barang').value = data.view_edit_barang.kode_barang;
                document.getElementById('merek_barang').value = data.view_edit_barang.merek;
                document.getElementById('no_seri').value = data.view_edit_barang.no_seri;
                document.getElementById('ukuran').value = data.view_edit_barang.ukuran;
                document.getElementById('bahan').value = data.view_edit_barang.bahan;
                document.getElementById('tahun_pembelian').value = data.view_edit_barang.thn_pembelian;
                document.getElementById('harga_barang').value = data.view_edit_barang.harga;

                console.log(data);
            }

        })
        console.log(kode);
    };

    function input_barangkan() {
        var data = $('#input_barang_kir').serialize();
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/Inventaris/simpan_barang_kir') ?>",
            data: data,
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Inventaris/add_barang/" . $view_add_barang_ruang->id_ruangan); ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Inventaris/add_barang/" . $view_add_barang_ruang->id_ruangan); ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);
    };

    // $.extend(true, $.fn.dataTable.defaults, {
    //     "searching": false,
    //     "ordering": true
    // });

    $(document).ready(function() {
        function rubah(angka) {
            var reverse = angka.toString().split('').reverse().join(''),
                ribuan = reverse.match(/\d{1,3}/g);
            ribuan = ribuan.join('.').split('').reverse().join('');
            return ribuan;
        }

        $("#barang_table_kir").dataTable({});
        $("#viewrecon").dataTable({
            "searching": false,

        });
    });

    $('#barang_table_kir tbody').on('click', '#recon_data_klik', function() {
        var currow = $(this).closest('tr');
        var col1 = currow.find('td:eq(1)').text();
        var col2 = currow.find('td:eq(2)').text();
        var col6 = currow.find('td:eq(6)').text();
        var result = col1 + '\n' + col2 + '\n' + col6 + '\n';
        // console.log(result);
        document.getElementById('id_kir_recon').value = col1;
        document.getElementById('jumlah_regis_recon').value = col6;
        document.getElementById('kode_barang_recon').value = col2;

    });


    function input_recon_kir() {

        var data = $('#input_recon').serialize();
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/Inventaris/simpan_recon_kir') ?>",
            data: data,
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Inventaris/add_barang/" . $view_add_barang_ruang->id_ruangan); ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Inventaris/add_barang/" . $view_add_barang_ruang->id_ruangan); ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);
    };

    $('#barang_table_kir tbody').on('click', '#view_recon_kirkan', function() {
        var currow = $(this).closest('tr');
        var col1 = currow.find('td:eq(1)').text();
        var col2 = currow.find('td:eq(2)').text();
        // alert(col2);
        document.getElementById('kode_barang_reconkankan').innerHTML = col2;
        view_recon_kir_kir();

        function view_recon_kir_kir() {
            console.log("tabelkna");
            $.ajax({
                type: 'AJAX',
                url: '<?php echo base_url('user/Inventaris/view_data_recon_kirkan/') ?>' + col1,
                async: false,
                dataType: 'json',
                success: function(data) {
                    var html = '';
                    var i;
                    var x = 1;
                    for (i = 0; i < data.view_recon_kir_kir.length; i++) {
                        html += '<tr>' +
                            '<td>' + [x++] + '</td>' +
                            '<td>' + data.view_recon_kir_kir[i].id_recon + '</td>' +
                            '<td>' + data.view_recon_kir_kir[i].kondisi_baik + '</td>' +
                            '<td>' + data.view_recon_kir_kir[i].kondisi_kurang_baik + '</td>' +
                            '<td>' + data.view_recon_kir_kir[i].kondisi_rusak + '</td>' +
                            '<td>' + data.view_recon_kir_kir[i].keterangan + '</td>' +
                            '<td>' + data.view_recon_kir_kir[i].tahun_recon + '</td>' +
                            '<td>' + '<a href="#" title="Hapus Recon" onclick="hapus_data_recon_tahunan(' + data.view_recon_kir_kir[i].id_recon + ')"><i class="md-icon material-icons" style="color:green;">delete</i></a>' +
                            '</td>' +

                            '</tr>';

                    }
                    $('#show_recon_kir').html(html);
                }

            });

        }

    });


    function hapus_data(kode) {
        // return confirm('Pastikan materi anda layak untuk di posting !!');
        var txt;
        var r = confirm("Anda yakin menghapus data ini!");
        if (r == true) {
            txt = "You pressed OK!";
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url('/user/Inventaris/hapus_Inventaris/') ?>' + kode,
                success: function(data) {

                    if (data == 'sukses') {
                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Berhasil Dihapus...',
                                title: '',
                                type: 'success',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Inventaris/add_barang/" . $view_add_barang_ruang->id_ruangan); ?>');
                        }, 1000);
                        console.log(data);

                    } else {

                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Gagal Dihapus...',
                                title: '',
                                type: 'error',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Inventaris/add_barang/" . $view_add_barang_ruang->id_ruangan); ?>');
                        }, 1000);
                    }
                    console.log(data);
                }

            })
        } else {
            txt = "You pressed Cancel!";
        }

        console.log(txt);
    };


    // batas

    function hapus_data_recon_tahunan(kode) {
        // return confirm('Pastikan materi anda layak untuk di posting !!');
        var txt;
        var r = confirm("Anda yakin menghapus data ini!");
        if (r == true) {
            txt = "You pressed OK!";
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url('/user/Inventaris/hapus_data_recon_tahunan/') ?>' + kode,
                success: function(data) {

                    if (data == 'sukses') {
                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Berhasil Dihapus...',
                                title: '',
                                type: 'success',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Inventaris/add_barang/" . $view_add_barang_ruang->id_ruangan); ?>');
                        }, 1000);
                        console.log(data);

                    } else {

                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Gagal Dihapus...',
                                title: '',
                                type: 'error',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Inventaris/add_barang/" . $view_add_barang_ruang->id_ruangan); ?>');
                        }, 1000);
                    }
                    console.log(data);
                }

            })
        } else {
            txt = "You pressed Cancel!";
        }

        console.log(txt);
    };


    $('#barang_table_kir tbody').on('click', '#edit_barang_kir_edit', function() {
        var currow = $(this).closest('tr');
        var col1 = currow.find('td:eq(1)').text();
        var col2 = currow.find('td:eq(2)').text();
        var col3 = currow.find('td:eq(3)').text();
        var col4 = currow.find('td:eq(4)').text();
        var col5 = currow.find('td:eq(5)').text();
        var col6 = currow.find('td:eq(6)').text();
        var col7 = currow.find('td:eq(7)').text();
        // console.log(result);
        document.getElementById('kode_barang_edit').value = col2;
        document.getElementById('nama_barang_edit').value = col3;
        document.getElementById('jumlah_regis_edit').value = col6;
        document.getElementById('keterangan_edit').value = col7;
        document.getElementById('kode_kir_edit').value = col1;
        // $('#pilih_user_edit').val(col4).change();

    });

    function input_barangkan_editkan() {

        var data = $('#input_edit_barang_kir').serialize();
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/Inventaris/simpan_edit_recon_kir') ?>",
            data: data,
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Inventaris/add_barang/" . $view_add_barang_ruang->id_ruangan); ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Inventaris/add_barang/" . $view_add_barang_ruang->id_ruangan); ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);

    }
</script>
<?php $this->load->view('user/layer/footer'); ?>