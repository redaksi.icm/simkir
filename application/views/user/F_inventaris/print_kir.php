<html>

<head>
    <title>Cetak PDF</title>
    <style>
        /*design table 1*/
        .table1 {
            /* font-family: sans-serif; */
            color: #444;
            font-size: 10px;
            border: 0px;
            width: 100%;
        }

        .table1 tr th {
            background: #dbdbdb;
            color: #040303;
        }

        #table1,
        th,
        td {
            padding: 5px 2px;
            font-size: 10px;
        }

        .table1 tr:hover {
            background-color: #040303;
        }

        .table1 tr:nth-child(even) {
            background-color: #040303;
        }

        .div {
            border: 2px solid black;
            width: 100%;
            height: 10%;
        }
    </style>

</head>

<body>
    <div>
        <table class="" align="center">
            <tr>
                <td>
                    <h3>LAPORAN KARTU INVENTARIS RUANGAN (KIR)</h3>
                </td>
            </tr>
        </table>
        <table class="" align="left">
            <tr>
                <td>PROVINSI</td>
                <td>:</td>
                <td>SUMATERA BARAT</td>
            </tr>
            <tr>
                <td>UNIT</td>
                <td>:</td>
                <td>SEKRETARIAT DAERAH</td>
            </tr>
            <tr>
                <td>SATUAN KERJA</td>
                <td>:</td>
                <td>BIRO UMUM</td>
            </tr>
            <tr>
                <td>GEDUNG</td>
                <td>:</td>
                <td><?php echo $ruangan->nama; ?></td>
            </tr>
            <tr>
                <td>RUANGAN</td>
                <td>:</td>
                <td><?php echo $ruangan->nama_ruangan; ?></td>
            </tr>
        </table>

        <br>
        <br>
        <div style="padding-left: 750px">NO_KODE LOKASI <?php echo $ruangan->kode_lokasi; ?></div>
        <table class="table1" border="0" align="center" id="table1">
            <tr style="text-align: center;">
                <th>No</th>
                <th style="width: 100px;">Nama Barang/ Jenis Barang</th>
                <th>Merek/ Model</th>
                <th>Nomor Seri/Pabrik</th>
                <th>Ukuran</th>
                <th>Bahan</th>
                <th style="width: 100px;">Tahun Pembuatan/Pembelian</th>
                <th>No. Kode Barang</th>
                <th style="width: 100px;">Jumlah Barang/ Register</th>
                <th style="width: 100px;">Harga Beli/ Perolehan (Rp)</th>
                <th>Baik</th>
                <th>Kurang Baik</th>
                <th>Rusak Berat</th>
                <th>Keterangan/ Mutasi</th>

            </tr>
            <tr>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>5</td>
                <td>6</td>
                <td>7</td>
                <td>8</td>
                <td>9</td>
                <td>10</td>
                <td>11</td>
                <td>12</td>
                <td>13</td>
                <td>14</td>
            </tr>
            <?php $no = 1;
            foreach ($data_barang as $value) { ?>
                <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $value->nama; ?></td>
                    <td><?php echo $value->merek; ?></td>
                    <td><?php echo $value->no_seri; ?></td>
                    <td><?php echo $value->ukuran; ?></td>
                    <td><?php echo $value->bahan; ?></td>
                    <td><?php echo $value->thn_pembelian; ?></td>
                    <td><?php echo $value->kode_barang; ?></td>
                    <td><?php echo $value->jumlah_register; ?></td>
                    <td><?php echo number_format($value->harga); ?></td>
                    <td><?php echo $value->kondisi_baik; ?></td>
                    <td><?php echo $value->kondisi_kurang_baik; ?></td>
                    <td><?php echo $value->kondisi_rusak; ?></td>
                    <td><?php echo $value->keterangan; ?></td>

                </tr>
            <?php } ?>
        </table>
        <table border="0" align="center" style="width: 100%;padding-top: 50px;">
            <tr>
                <td style="width: 30%;text-align: center;">Mengetahui</td>
                <td style="width: 30%;text-align: center;"></td>
                <td style="width: 30%;text-align: center;">Padang, <?php echo date('F Y');?></td>
            </tr>
            <tr>
                <td style="width: 30%;text-align: center;">PEJABAT PENATAUSAHAAN PENGGUNA BARANG MILIK DAERAH</td>
                <td style="width: 30%;text-align: center;">PENGURUS BARANG BIRO UMUM</td>
                <td style="width: 30%;text-align: center;">PETUGAS <?php echo $ruangan->nama_ruangan; ?></td>
            </tr>
            <tr>
                <td style="width: 30%;text-align: center;padding-top: 60px;">KUSUMA DEWI, M.I.Kom</td>
                <td style="width: 30%;text-align: center;padding-top: 60px;">ISMAIL</td>
                <td style="width: 30%;text-align: center;padding-top: 60px;">FIRDAUS,A.MD</td>
            </tr>
            <tr>
                <td style="width: 30%;text-align: center;">NIP. 198609201004122001</td>
                <td style="width: 30%;text-align: center;">NIP. 197408162007011005</td>
                <td style="width: 30%;text-align: center;">NIP. 197502022009011004</td>
            </tr>
        </table>
    </div>

</body>

</html>