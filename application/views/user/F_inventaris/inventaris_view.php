<?php $this->load->view('user/layer/header') ?>
<!-- tempat css/javascript -->
<style>
    /* kosong */
</style>
<?php $this->load->view('user/layer/body_atas') ?>

<div id="page_content_inner">
    <h3 class="heading_b uk-margin-bottom">DASHBOARD > KARTU INVENTARIS RUANGAN [KIR]</h3>
    <div class="md-card">
        <div class="md-card-content">
            <table id="" class="uk-table" cellspacing="0" style="width: 400px;max-width: 50%;">
                <tr>
                    <td>PROVINSI</td>
                    <td>:</td>
                    <td>SUMATERA BARAT</td>
                </tr>
                <tr>
                    <td>UNIT</td>
                    <td>:</td>
                    <td>SEKRETARIAT DAERAH</td>
                </tr>
                <tr>
                    <td>SATUAN KERJA</td>
                    <td>:</td>
                    <td>BIRO UMUM</td>
                </tr>
            </table>
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="ruangan_table" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Ruangan</th>
                                <th>Nama Gedung</th>
                                <th>Kode Lokasi</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="show_ruangan" class="show_ruangan">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('user/layer/body_bawah'); ?>
<script src="<?php echo base_url('/assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<!-- datatables colVis-->
<script src="<?php echo base_url('/assets/bower_components/datatables-colvis/js/dataTables.colVis.js') ?>"></script>
<!-- datatables tableTools-->
<script src="<?php echo base_url('/assets/bower_components/datatables-tabletools/js/dataTables.tableTools.js') ?>"></script>
<!-- datatables custom integration -->
<script src="<?php echo base_url('/assets/assets/js/custom/datatables_uikit.min.js') ?>"></script>

<!--  datatables functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/plugins_datatables.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/pages/components_notifications.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/kendoui_custom.min.js') ?>"></script>

<!--  kendoui functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/kendoui.min.js') ?>"></script>


<!-- tempat javascript -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#ruangan_table").dataTable();
        view_ruangan();

        function view_ruangan() {
            $.ajax({
                type: 'AJAX',
                url: '<?php echo base_url() ?>user/Ruang/view_data_ruangan',
                async: false,
                dataType: 'json',
                success: function(data) {
                    var html = '';
                    var i;
                    var x = 1;
                    for (i = 0; i < data.ruangan.length; i++) {
                        html += '<tr>' +
                            '<td>' + [x++] + '</td>' +
                            '<td>' + data.ruangan[i].nama_ruangan + '</td>' +
                            '<td>' + data.ruangan[i].nama + '</td>' +
                            '<td>' + data.ruangan[i].kode_lokasi + '</td>' +
                            '<td class="">' +
                            '<a class="md-btn md-btn-danger md-btn-small md-btn-wave-light waves-effect waves-button waves-light" href="<?php echo base_url("user/Inventaris/add_barang/") ?>' + data.ruangan[i].id_ruangan + '"><i class="material-icons" style="color:blanchedalmond;">add_box</i> Barang</a>' +
                            // '<a href="#" title="Hapus Recon" onclick="hapus_data("'+ data.ruangan[i].nama_ruangan +'")"><i class="md-icon material-icons" style="color:green;">delete</i></a>'+
                            '</td>' +
                            '</tr>';

                    }
                    $('#show_ruangan').html(html);
                }

            });
        };

    });
</script>
<?php $this->load->view('user/layer/footer'); ?>