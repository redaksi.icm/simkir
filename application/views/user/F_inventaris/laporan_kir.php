<?php $this->load->view('user/layer/header') ?>
<!-- tempat css/javascript -->
<style>
    /* kosong */
</style>
<?php $this->load->view('user/layer/body_atas') ?>

<div id="page_content_inner">
    <h3 class="heading_b uk-margin-bottom">DASHBOARD > LAPORAN [KIR]</h3>
    <div class="md-card">
        <div class="md-card-content">
            <table id="" class="uk-table" cellspacing="0" style="width: 100%;max-width: 60%;">
                <tr>
                    <td style="width: 40%;">PROVINSI</td>
                    <td>:</td>
                    <td>SUMATERA BARAT</td>
                </tr>
                <tr>
                    <td>UNIT</td>
                    <td>:</td>
                    <td>SEKRETARIAT DAERAH</td>
                </tr>
                <tr>
                    <td>SATUAN KERJA</td>
                    <td>:</td>
                    <td>BIRO UMUM</td>
                </tr>
                <tr>
                    <td>CARI RUANGA | GEDUNG</td>
                    <td>:</td>
                    <td>
                        <div class="uk-width-medium-2-2">
                            <select id="val_select" class="gedungan" required data-md-selectize>
                                <option value="0">Pilih...</option>
                                <?php
                                foreach ($data_gedung as $value_gedung) {
                                ?>

                                    <option value="<?php echo $value_gedung->id_ruangan; ?>"><?php echo $value_gedung->nama_ruangan; ?> (<?php echo $value_gedung->nama; ?>)</option>
                                <?php } ?>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>PILIH TAHUN</td>
                    <td>:</td>
                    <td>
                        <div class="row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    <select id="val_select" class="tahunan" required data-md-selectize>
                                        <option value="0">Pilih...</option>
                                        <?php
                                        date_default_timezone_set('Asia/Jakarta');
                                        $tahunow = date('Y');
                                        for ($tahun = 2015; $tahun <= $tahunow; $tahun++) {
                                            echo "<option  value='$tahun'>$tahun </option>";
                                        }

                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>PILIH KONDISI</td>
                    <td>:</td>
                    <td>
                        <div class="row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2">
                                    <select id="val_select" class="kondisi" required data-md-selectize>
                                        <option value="0">Pilih...</option>
                                        <option value="semua">Semua Kondisi</option>
                                        <option value="baik">Baik</option>
                                        <option value="kurang">Kurang Baik</option>
                                        <option value="rusak">Kondisi Rusak</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="uk-width-medium-1-1" style="width: none;">
                <a onclick="showkan_data_kir()" class="md-btn md-btn-danger md-btn-wave-light waves-effect waves-button waves-light" href="#">Cari</a>
                <a onclick="print_data_kir()" class="md-btn md-btn-danger md-btn-wave-light waves-effect waves-button waves-light" href="#">Print</a>
            </div>
            <br>
            <br>
            <table border="0">
                <tr>
                    <td>
                        <div class="md-input-wrapper md-input-filled">
                            <input type="text" name="kode_ruangan" id="kode_ruangan" style="border: 1px solid;" placeholder="Cari Kode Barang" class="md-input kode_ruangan">
                            <span class="md-input-bar"></span>
                        </div>
                    </td>
                    <td>
                        <a onclick="Caribarangkan()" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light" href="#">Cari Barang</a></td>
                </tr>
            </table>
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="kir_view_kan" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Tahun Pembelian</th>
                                <th>Jumlah Register</th>
                                <th>Harga</th>
                                <th>Baik</th>
                                <th>Kurang Baik</th>
                                <th>Kondisi Rusak</th>
                                <th>Ket</th>
                            </tr>
                        </thead>
                        <tbody id="show_recon_kir">
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('user/layer/body_bawah'); ?>
<script src="<?php echo base_url('/assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<!-- datatables colVis-->
<script src="<?php echo base_url('/assets/bower_components/datatables-colvis/js/dataTables.colVis.js') ?>"></script>
<!-- datatables tableTools-->
<script src="<?php echo base_url('/assets/bower_components/datatables-tabletools/js/dataTables.tableTools.js') ?>"></script>
<!-- datatables custom integration -->
<script src="<?php echo base_url('/assets/assets/js/custom/datatables_uikit.min.js') ?>"></script>

<!--  datatables functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/plugins_datatables.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/pages/components_notifications.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/kendoui_custom.min.js') ?>"></script>

<!--  kendoui functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/kendoui.min.js') ?>"></script>


<!-- tempat javascript -->
<script type="text/javascript">
    $(document).ready(function() {
        // $("#kir_view_kan").dataTable();
        // $("#kir_view_kan").dataTable();
        // $.extend(true, $.fn.dataTable.defaults, {
        //     "searching": false
        // });
    });

    function Caribarangkan() {
        showkan_data_kir(1);
    }


    function showkan_data_kir(sesion) {
        var kode = $('.gedungan').val();
        var tahun = $('.tahunan').val();
        var kondisi = $('.kondisi').val();
        var kode_barang = $('#kode_ruangan').val();

        if (kode == '' || tahun == '' || kondisi == 0) {
            alert('Jangan Kosongkan pilihan-pilihan di atas jika mau mencari data');
        }


        if (sesion == 1) {
            console.log('cari kode');
            url = '<?php echo base_url('user/Laporan_kir/viewkan_kir/') ?>' + kode + '/' + tahun + '/' + kondisi + '/' + kode_barang
        } else {
            console.log('indak cari kode');
            url = '<?php echo base_url('user/Laporan_kir/viewkan_kir/') ?>' + kode + '/' + tahun + '/' + kondisi + '/' + 0
        }


        console.log(kode, tahun);
        var html = '';
        $.ajax({
            type: 'AJAX',
            url: url,
            async: false,
            dataType: 'json',
            success: function(data) {
                var i;
                var x = 1;
                for (i = 0; i < data.data_gedung.length; i++) {
                    console.log(data.data_gedung);
                    // if (data.data_gedung === ' ' || data.data_gedung === null) {
                    //     html += '<tr><td colspan="10">DATA KOSONG </td></tr>';
                    // } else {
                    html += '<tr>' +
                        '<td>' + [x++] + '</td>' +
                        '<td>' + data.data_gedung[i].kode_barang + '</td>' +
                        '<td>' + data.data_gedung[i].nama + '</td>' +
                        '<td>' + data.data_gedung[i].thn_pembelian + '</td>' +
                        '<td>' + data.data_gedung[i].jumlah_register + '</td>' +
                        '<td>' + data.data_gedung[i].harga + '</td>' +
                        '<td>' + data.data_gedung[i].kondisi_baik + '</td>' +
                        '<td>' + data.data_gedung[i].kondisi_kurang_baik + '</td>' +
                        '<td>' + data.data_gedung[i].kondisi_rusak + '</td>' +
                        '<td>' + data.data_gedung[i].keterangan + '</td>' +
                        '</tr>';
                    // }


                }
                $('#show_recon_kir').html(html);
            }

        });

    };

    function print_data_kir() {
        var gedungan = $('.gedungan').val();
        var tahun = $('.tahunan').val();
        console.log(tahun);
        url = "<?php echo base_url('user/Inventaris/print_kir'); ?>";
        url = url + "/" + tahun + '/' + gedungan;
        var win = window.open(url, '_blank');
        win.focus();

    }
</script>
<?php $this->load->view('user/layer/footer'); ?>