<?php $this->load->view('user/layer/header') ?>
<!-- tempat css/javascript -->
<link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/kendo-ui/styles/kendo.common-material.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/kendo-ui/styles/kendo.material.min.css') ?>" />
<style>
    /* kosong */
</style>
<?php $this->load->view('user/layer/body_atas') ?>

<div id="page_content_inner">
    <h3 class="heading_b uk-margin-bottom">DASHBOARD > PERBAIKAN BARANG</h3>
    <div class="md-card">
        <h3 class="uk-text-center">Pilih Barang yang Akan Perbaikan</h3>
        <div class="md-card-content">
            <table id="" class="uk" cellspacing="0" style="width: 100%;max-width: 100%;font-size: 17px;">
                <tr>
                    <td style="width: 10%;">TENTANG</td>
                    <td style="width: 1%;">:</td>
                    <td>
                        <?php echo $perbaikan->tentang; ?>
                        <input type="text" name="id_perbaikan" id="id_perbaikan" value="<?php echo $perbaikan->id_perbaikan; ?>">
                    </td>
                </tr>
                <tr>
                    <td>NOMOR</td>
                    <td>:</td>
                    <td><?php echo $perbaikan->nomor_perbaikan; ?></td>
                </tr>
                <tr>
                    <td>TANGGAL</td>
                    <td>:</td>
                    <td><?php echo $perbaikan->tanggal; ?></td>
                </tr>
            </table>
        </div>

        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <label for="" style="color: red;">PILIH BARANG..</label>
                                    <select id="val_select" class="kode_barangg" required data-md-selectize style="width: 100%;">
                                        <option value="0">Pilih...</option>
                                        <?php
                                        foreach ($barang_add as $value_brg) {
                                        ?>
                                            <option value="<?php echo $value_brg->id; ?>"><?php echo $value_brg->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="uk-width-medium-2-2">
                                    <br><br>
                                    <label for="" style="color: red;">PILIH RUANGAN..</label>
                                    <select id="val_select" class="kode_ruangan" required data-md-selectize style="width: 100%;">
                                        <option value="0">Pilih...</option>
                                        <?php
                                        foreach ($ruangan as $ruangan1) {
                                        ?>
                                            <option value="<?php echo $ruangan1->id_ruangan; ?>"><?php echo $ruangan1->nama_ruangan; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="uk-width-medium-1-2">
                                    <br>
                                    <a onclick="tambahkan_barang_perbaikan()" class="md-btn md-btn-danger" href="#">Tambah Barang</a>
                                </div>
                                <div class="uk-width-medium-1-2">
                                    <br>
                                    <a class="md-btn md-btn-success" href="#" style="width: max-content;" title="Tambah Gedung" onclick="print_data_perbaikan()">Print PERBAIKAN </a>
                                </div>
                                <!-- <div class="uk-width-medium-1-2">
                                    <br>
                                    <a onclick="simpan_perbaikan()" class="md-btn md-btn-danger" href="#">Simpan Perbaikan</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <br>
                                    <label>JUMLAH RUSAK</label>
                                    <input type="text" name="jumlah" id="jumlah" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>KETERANGAN</label>
                                    <textarea cols="30" rows="4" class="md-input" name="keterangan" id="keterangan"></textarea>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <table id="mutasi_table" class="uk-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <!-- <th>Kode Barang</th> -->
                        <th>Nama Barang</th>
                        <th>Ruangan</th>
                        <th>Rusak</th>
                        <th>Ket</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <?php $no = 1;
                foreach ($view_barang_perbaikan as $value) { ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <!-- <td><?php echo $value->kode_barang; ?></td> -->
                        <td><?php echo $value->nama; ?></td>
                        <td><?php echo $value->nama_ruangan; ?></td>
                        <td><?php echo $value->jumlah; ?></td>
                        <td><?php echo $value->ket; ?></td>
                        <td>
                            <a href="#" title="Hapus" onclick="hapus_data(<?php echo $value->id_barang_perbaikan; ?>)"><i class="md-icon material-icons" style="color:green;">delete</i></a>

                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>

</div>

<?php $this->load->view('user/layer/body_bawah'); ?>
<script src="<?php echo base_url('/assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<!-- datatables colVis-->
<script src="<?php echo base_url('/assets/bower_components/datatables-colvis/js/dataTables.colVis.js') ?>"></script>
<!-- datatables tableTools-->
<script src="<?php echo base_url('/assets/bower_components/datatables-tabletools/js/dataTables.tableTools.js') ?>"></script>
<!-- datatables custom integration -->
<script src="<?php echo base_url('/assets/assets/js/custom/datatables_uikit.min.js') ?>"></script>

<!--  datatables functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/plugins_datatables.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/pages/components_notifications.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/kendoui_custom.min.js') ?>"></script>

<!--  kendoui functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/kendoui.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js') ?>"></script>
<!-- htmleditor (codeMirror) -->
<script src="<?php echo base_url('/assets/assets/js/uikit_htmleditor_custom.min.js'); ?>"></script>
<!-- inputmask-->
<script src="<?php echo base_url('/assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js') ?>"></script>

<!--  forms advanced functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/forms_advanced.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/bower_components/parsleyjs/dist/parsley.min.js') ?>"></script>
<!-- jquery steps -->
<script src="<?php echo base_url('/assets/assets/js/custom/wizard_steps.min.js') ?>"></script>

<!--  forms wizard functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/forms_wizard.min.js') ?>"></script>
<script>
    $('#mutasi_table').dataTable();

    function tambahkan_barang_perbaikan() {
        var id_barang = $('.kode_barangg option:selected').attr("value");
        var id_perbaikan = $('#id_perbaikan').val();
        var keterangan = $('#keterangan').val();
        var kode_ruangan = $('.kode_ruangan').val();
        var jumlah = $('#jumlah').val();


        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/Perbaikan/simpan_barang_perbaikan') ?>",
            data: {
                id_barang: id_barang,
                id_perbaikan: id_perbaikan,
                keterangan: keterangan,
                kode_ruangan: kode_ruangan,
                jumlah: jumlah
            },
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Perbaikan/add_perbiakan/") . $perbaikan->id_perbaikan; ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Perbaikan/add_perbiakan/") . $perbaikan->id_perbaikan; ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);

    }

    function hapus_data(kode) {
        var txt;
        var r = confirm("Anda yakin menghapus data ini!");
        if (r == true) {
            txt = "You pressed OK!";
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url('/user/Perbaikan/hapus_barangcoy/') ?>' + kode,
                success: function(data) {

                    if (data == 'sukses') {
                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Berhasil Dihapus...',
                                title: '',
                                type: 'success',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Perbaikan/add_perbiakan/") . $perbaikan->id_perbaikan; ?>');
                        }, 1000);
                        console.log(data);

                    } else {

                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Gagal Dihapus...',
                                title: '',
                                type: 'error',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Perbaikan/add_perbiakan/") . $perbaikan->id_perbaikan; ?>');
                        }, 1000);
                    }
                    console.log(data);
                }

            })
            console.log(kode);
        } else {

        }
    }


    function print_data_perbaikan() {
        var id_perbaikan = $('#id_perbaikan').val();
        url = "<?php echo base_url('user/Perbaikan/print_Perbaikan'); ?>";
        url = url + '/' + id_perbaikan;
        var win = window.open(url, '_blank');
        win.focus();

    }
</script>
<?php $this->load->view('user/layer/footer'); ?>