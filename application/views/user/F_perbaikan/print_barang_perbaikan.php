<html>

<head>
    <title>Cetak PDF</title>
    <style>
        /*design table 1*/
        .table1 {
            /* font-family: sans-serif; */
            color: #444;
            font-size: 10px;
            border: 0px;
            width: 100%;
        }

        .table1 tr th {
            background: #dbdbdb;
            color: #040303;
        }

        #table1,
        th,
        td {
            padding: 5px 2px;
            font-size: 10px;
        }

        .table1 tr:hover {
            background-color: #040303;
        }

        .table1 tr:nth-child(even) {
            background-color: #040303;
        }

        .div {
            border: 2px solid black;
            width: 100%;
            height: 10%;
        }
    </style>

</head>

<body>
    <div>
        <table class="" align="center">
            <tr>
                <td>
                    <h3>LAPORAN ENGHAPUSAN BARANG</h3>
                </td>
            </tr>
        </table>
        <table class="" align="left">
            <tr>
                <td>PROVINSI</td>
                <td>:</td>
                <td>SUMATERA BARAT</td>
            </tr>
            <tr>
                <td>UNIT</td>
                <td>:</td>
                <td>SEKRETARIAT DAERAH</td>
            </tr>
            <tr>
                <td>SATUAN KERJA</td>
                <td>:</td>
                <td>BIRO UMUM</td>
            </tr>

            <tr>
                <td>TENTANG</td>
                <td>:</td>
                <td><?php echo $perbaikan->tentang; ?></td>
            </tr>
            <tr>
                <td>NOMOR</td>
                <td>:</td>
                <td><?php echo $perbaikan->nomor_perbaikan; ?></td>
            </tr>
            <tr>
                <td>TANGGAL</td>
                <td>:</td>
                <td><?php echo $perbaikan->tanggal; ?></td>
            </tr>
        </table>

        <table class="table1" border="0" align="center" id="table1">
            <tr>
                <th>No</th>
                <th style="width: 25%;">Nama Barang</th>
                <th style="width: 25%;">Ruangan</th>
                <th style="width: 25%;">Rusak</th>
                <th style="width: 25%;">Ket</th>
            </tr>
            <tr>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>5</td>
            </tr>
            <?php $no = 1;
            foreach ($view_barang_perbaikan as $value) { ?>
                <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $value->nama; ?></td>
                    <td><?php echo $value->nama_ruangan; ?></td>
                    <td><?php echo $value->jumlah; ?></td>
                    <td><?php echo $value->ket; ?></td>
                </tr>


            <?php } ?>
        </table>

        <br>
        <br>
        <table border="0" align="center" style="width: 100%;padding-top: 50px;">
            <tr>
                <td style="width: 30%;text-align: center;">Mengetahui</td>
                <td style="width: 30%;text-align: center;"></td>
                <td style="width: 30%;text-align: center;">Padang, <?php echo date('F Y'); ?></td>
            </tr>
            <tr>
                <td style="width: 30%;text-align: center;">PEJABAT PENATAUSAHAAN PENGGUNA BARANG MILIK DAERAH</td>
                <td style="width: 30%;text-align: center;">PENGURUS BARANG BIRO UMUM</td>
                <td style="width: 30%;text-align: center;">PETUGAS </td>
            </tr>
            <tr>
                <td style="width: 30%;text-align: center;padding-top: 60px;">KUSUMA DEWI, M.I.Kom</td>
                <td style="width: 30%;text-align: center;padding-top: 60px;">ISMAIL</td>
                <td style="width: 30%;text-align: center;padding-top: 60px;">FIRDAUS,A.MD</td>
            </tr>
            <tr>
                <td style="width: 30%;text-align: center;">NIP. 198609201004122001</td>
                <td style="width: 30%;text-align: center;">NIP. 197408162007011005</td>
                <td style="width: 30%;text-align: center;">NIP. 197502022009011004</td>
            </tr>
        </table>




    </div>
</body>

</html>