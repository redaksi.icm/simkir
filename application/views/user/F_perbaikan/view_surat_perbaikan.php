<?php $this->load->view('user/layer/header') ?>
<!-- tempat css/javascript -->
<link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/kendo-ui/styles/kendo.common-material.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/kendo-ui/styles/kendo.material.min.css') ?>" />
<style>
    /* kosong */
</style>
<?php $this->load->view('user/layer/body_atas') ?>

<div id="page_content_inner">
    <h3 class="heading_b uk-margin-bottom">DASHBOARD > PERBAIKAN BARANG</h3>
    <div class="md-card">
        <div class="md-card-content">
            <!-- <h3>DATA BARANG YANG PERBAIKAN SESUAI KEPUTUSAN GUBERNUR</h3> -->
            <div class="md-card uk-margin-medium-bottom">
                <div class="uk-width-medium-1-6" style="padding-left: 850px;">
                    <a class="md-btn md-btn-primary" href="#" style="width: max-content;" title="Tambah Data" data-uk-modal="{target:'#tambah_keputusan'}">Tambah Data</a>
                </div>
                <div class="md-card-content">
                    <table id="mutasi_table" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Perbaikan</th>
                                <th style="width: 200px;">Tentang</th>
                                <th>Tanggal</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($perbaikan as $value) { ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $value->nomor_perbaikan; ?></td>
                                    <td><?php echo $value->tentang; ?></td>
                                    <td><?php echo $value->tanggal; ?></td>
                                    <td style="width: 100px;">
                                        <?php if ($value->status == 'Lihat') { ?>
                                            <div class="uk-width-medium-1-2">
                                                <a href="<?php echo base_url('user/Perbaikan/add_perbiakan/') . $value->id_perbaikan; ?>" class="md-btn md-btn-danger">Tambah Barang</a>
                                                <a href="#" title="Hapus" onclick="hapus_data(<?php echo $value->id_perbaikan; ?>)"><i class="md-icon material-icons" style="color:green;">delete</i></a>

                                            </div>
                                        <?php } else { ?>
                                            <!-- <div class="uk-width-medium-1-2">
                                                <a href="#" class="md-btn md-btn-danger">Lihat Data</a>
                                            </div> -->
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="md-fab-wrapper">
    <a class="md-fab md-fab-success" href="#" data-uk-tooltip="{pos:'top'}" title="Tambah" data-uk-modal="{target:'#tambah_keputusan'}"><i class="material-icons">playlist_add</i></a>
</div> -->

<div class="uk-modal" id="tambah_keputusan">
    <form action="" method="POST" id="input_keputusan">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Nota Dinas</h3>
            </div>
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-2-2">
                        <div class="uk-form-row">
                            <div class="uk-grid">
                                <div class="uk-width-medium-2-2">
                                    <label>NOMOR PERBAIKAN</label><br>
                                    <input type="text" name="nomor" id="nomor" required class="md-input label-fixed" />
                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>TENTANG</label>
                                    <textarea cols="30" rows="4" class="md-input" name="tetang" id="tetang"></textarea>

                                </div>
                                <div class="uk-width-medium-2-2">
                                    <br>
                                    <label>TANGGAL</label>
                                    <br>
                                    <br>
                                    <input id="tanggal" name="tanggal" value="" style="width: fit-content;" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close" onclick="input_keputusan()">SIMPAN</button>
            </div>
        </div>
    </form>
</div>

<?php $this->load->view('user/layer/body_bawah'); ?>
<script src="<?php echo base_url('/assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<!-- datatables colVis-->
<script src="<?php echo base_url('/assets/bower_components/datatables-colvis/js/dataTables.colVis.js') ?>"></script>
<!-- datatables tableTools-->
<script src="<?php echo base_url('/assets/bower_components/datatables-tabletools/js/dataTables.tableTools.js') ?>"></script>
<!-- datatables custom integration -->
<script src="<?php echo base_url('/assets/assets/js/custom/datatables_uikit.min.js') ?>"></script>

<!--  datatables functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/plugins_datatables.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/pages/components_notifications.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/assets/js/kendoui_custom.min.js') ?>"></script>

<!--  kendoui functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/kendoui.min.js') ?>"></script>
<script src="<?php echo base_url('/assets/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js') ?>"></script>
<!-- htmleditor (codeMirror) -->
<script src="<?php echo base_url('/assets/assets/js/uikit_htmleditor_custom.min.js'); ?>"></script>
<!-- inputmask-->
<script src="<?php echo base_url('/assets/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js') ?>"></script>

<!--  forms advanced functions -->
<script src="<?php echo base_url('/assets/assets/js/pages/forms_advanced.min.js') ?>"></script>

<script>
    $(document).ready(function() {
        $("#mutasi_table").dataTable({});
    });
    $(document).ready(function() {
        // $("#mutasi_table").dataTable({});
    });
    $('#tanggal').kendoDatePicker({
        format: "yyyy-MM-d"
    });

    function input_keputusan() {

        var data = $('#input_keputusan').serialize();
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/Perbaikan/simpan_perbaikan') ?>",
            data: data,
            success: function(data) {

                if (data == 'sukses') {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Berhasil Disimpan...',
                            title: '',
                            type: 'success',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Perbaikan"); ?>');
                    }, 1000);
                    console.log(data);

                } else {

                    setTimeout(function() {
                        swal({
                            position: 'top-end',
                            text: 'Gagal Disimpan...',
                            title: '',
                            type: 'error',
                            timer: 1500,
                            showConfirmButton: true
                        });
                    }, 10);
                    window.setTimeout(function() {
                        window.location.replace('<?php echo base_url("user/Perbaikan"); ?>');
                    }, 1000);
                }

            },

        });
        console.log(data);

    };

    function hapus_data(kode) {
        var txt;
        var r = confirm("Anda yakin menghapus data ini!");
        if (r == true) {
            txt = "You pressed OK!";
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url('/user/Perbaikan/hapus_Perbaikancoy/') ?>' + kode,
                success: function(data) {

                    if (data == 'sukses') {
                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Berhasil Dihapus...',
                                title: '',
                                type: 'success',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Perbaikan"); ?>');
                        }, 1000);
                        console.log(data);

                    } else {

                        setTimeout(function() {
                            swal({
                                position: 'top-end',
                                text: 'Gagal Dihapus...',
                                title: '',
                                type: 'error',
                                timer: 1500,
                                showConfirmButton: true
                            });
                        }, 10);
                        window.setTimeout(function() {
                            window.location.replace('<?php echo base_url("user/Perbaikan") ?>');
                        }, 1000);
                    }
                    console.log(data);
                }

            })
            console.log(kode);
        } else {

        }
    }
</script>

<?php $this->load->view('user/layer/footer'); ?>