<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no" />

    <link rel="icon" type="image/png" href="<?php echo base_url('/assets/gambar/favicon.png') ?>" sizes="16x16">
    <link rel="icon" type="image/png" href="<?php echo base_url('/assets/gambar/favicon.png') ?>" sizes="32x32">

    <title>LOGIN | SIMKIR KANTOR GUBERNUR SUMATERA BARAT</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>

    <!-- uikit -->
    <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/uikit/css/uikit.almost-flat.min.css'); ?>" />

    <!-- altair admin login page -->
    <link rel="stylesheet" href="<?php echo base_url('/assets/assets/css/login_page.min.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('/assets/assets/icons/flags/flags.min.css') ?>" media="all">

    <!-- altair admin -->
    <link rel="stylesheet" href="<?php echo base_url('/assets/assets/css/main.min.css'); ?>" media="all">

</head>

<body class="login_page" style="background-image: url('<?php echo base_url('/assets/gambar/kantor_gubernur.jpg') ?>');background-size:cover;background-attachment: fixed;color:white;">

    <div class="login_page_wrapper">
        <div class="md-card" id="login_card">
            <div class="md-card-content large-padding" id="login_form">
                <div class="login_heading">
                    <div class="user_avatar">
                        <img class="" src="<?php echo base_url('/assets/gambar/favicon.png') ?>" alt="">
                    </div>
                </div>
                <?php

                if (($this->session->flashdata('gagal')) == 'error_login') {
                    echo '<ul class="md-list md-list-addon"><li>
                            <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons uk-text-warning"></i>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading" style="color: black;">Gagal Login...</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">Silahkan cek lagi akun anda...</span>
                            </div>
                        </li></ul>';
                } else {
                }
                ?>
                <form method="POST" action="<?php echo base_url('Login/aksi_login') ?>">
                    <div class="uk-form-row">
                        <label for="login_username">NIP</label>
                        <input class="md-input" type="text" id="login_username" name="username_nip" />
                    </div>
                    <div class="uk-form-row">
                        <label for="login_password">Password</label>
                        <input class="md-input" type="password" id="login_password" name="password" />
                    </div>
                    <div class="uk-margin-medium-top">
                        <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign In</button>
                    </div>
                    <div class="uk-margin-top">
                        <a href="#" id="login_help_show" class="uk-float-right">Need help?</a>
                        <span class="icheck-inline">
                            <input type="checkbox" name="login_page_stay_signed" id="login_page_stay_signed" data-md-icheck />
                            <label for="login_page_stay_signed" class="inline-label">Stay signed in</label>
                        </span>
                    </div>
                </form>
            </div>
            <div class="md-card-content large-padding uk-position-relative" id="login_help" style="display: none">
                <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                <h2 class="heading_b uk-text-success">Can't log in?</h2>
                <p>Here’s the info to get you back in to your account as quickly as possible.</p>
                <p>First, try the easiest thing: if you remember your password but it isn’t working, make sure that Caps Lock is turned off, and that your username is spelled correctly, and then try again.</p>
                <p>If your password still isn’t working, it’s time to <a href="#" id="password_reset_show">reset your password</a>.</p>
            </div>
            <div class="md-card-content large-padding" id="login_password_reset" style="display: none">
                <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                <h2 class="heading_a uk-margin-large-bottom">Reset password</h2>
                <form>
                    <div class="uk-form-row">
                        <label for="login_email_reset">Your email address</label>
                        <input class="md-input" type="text" id="login_email_reset" name="login_email_reset" />
                    </div>
                    <div class="uk-margin-medium-top">
                        <a href="index.html" class="md-btn md-btn-primary md-btn-block">Reset password</a>
                    </div>
                </form>
            </div>
            <div class="md-card-content large-padding" id="register_form" style="display: none">
                <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                <h2 class="heading_a uk-margin-medium-bottom">Create an account</h2>
                <form>
                    <div class="uk-form-row">
                        <label for="register_username">Username</label>
                        <input class="md-input" type="text" id="register_username" name="register_username" />
                    </div>
                    <div class="uk-form-row">
                        <label for="register_password">Password</label>
                        <input class="md-input" type="password" id="register_password" name="register_password" />
                    </div>
                    <div class="uk-form-row">
                        <label for="register_password_repeat">Repeat Password</label>
                        <input class="md-input" type="password" id="register_password_repeat" name="register_password_repeat" />
                    </div>
                    <div class="uk-form-row">
                        <label for="register_email">E-mail</label>
                        <input class="md-input" type="text" id="register_email" name="register_email" />
                    </div>
                    <div class="uk-margin-medium-top">
                        <a href="index.html" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign Up</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="uk-margin-top uk-text-center">
            <a href="#" id="signup_form_show">SIMKIR KANTOR GUBERNUR SUMATERA BARAT</a>
        </div>
    </div>

    <!-- common functions -->
    <script src="<?php echo base_url('/assets/assets/js/common.min.js') ?>"></script>
    <!-- altair core functions -->
    <script src="<?php echo base_url('/assets/assets/js/altair_admin_common.min.js') ?>"></script>
    <script src="<?php echo base_url('/assets/assets/js/uikit_custom.min.js') ?>"></script>

    <!-- altair login page functions -->
    <script src="<?php echo base_url('/assets/assets/js/pages/login.min.js'); ?>"></script>

    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-65191727-1', 'auto');
        ga('send', 'pageview');
    </script>
</body>

</html>